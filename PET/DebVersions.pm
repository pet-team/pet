# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Routines for comparing package versions, based on policy + dpkg code
# I'm not using AptPkg::Version since it depends on having a working apt and
# dpkg, it's overly complicated and underdocumented.
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package PET::DebVersions;
use strict;
use warnings;
use Carp;

our @ISA = "Exporter";
our @EXPORT = qw( deb_compare deb_compare_nofail deb_valid );

sub deb_valid($) {
    my $v = shift;
    return (defined $v and
        $v =~ /^(?:(\d+):)?([A-Za-z0-9+.:~_-]*?)(?:-([+.~_A-Za-z0-9]+))?$/);
}
sub deb_parse($) {
    my $v = shift;
    unless(defined $v) {
        carp "Empty debian package version passed";
        return ();
    }
    unless($v =~ /^(?:(\d+):)?([A-Za-z0-9+.:~_-]*?)(?:-([+.~_A-Za-z0-9]+))?$/) {
        warn "Invalid debian package version: $v\n";
        return ();
    };
    return($1 || 0, $2, $3 || "");
}
sub dpkg_order($) {
    my $v = shift;
    return 0 if (! defined($v) or $v =~ /[0-9]/);
    return -1 if ($v eq '~');
    return ord($v) if ($v =~ /[a-zA-Z]/);
    return ord($v) + 256;
}
sub deb_verrevcmp($$) {
    my($a, $b) = @_;
    my($x, $y);
    while(length($a) or length($b)) {
        while(1) {
            $x = length($a) ? substr($a, 0, 1) : undef;
            $y = length($b) ? substr($b, 0, 1) : undef;
            last unless((defined $x and $x =~ /\D/) or
                (defined $y and $y =~ /\D/));
            my $r = dpkg_order($x) <=> dpkg_order($y);
            return $r if($r);
            substr($a, 0, 1, "") if(defined $x);
            substr($b, 0, 1, "") if(defined $y);
        }
        $a =~ s/^(\d*)//;
        $x = $1 || 0;
        $b =~ s/^(\d*)//;
        $y = $1 || 0;
        my $r = $x <=> $y;
        return $r if($r);
    }
    return 0;
}
sub deb_compare($$) {
    return undef unless(deb_valid($_[0]) and deb_valid($_[1]));
    my @va = deb_parse($_[0]);
    my @vb = deb_parse($_[1]);

    # Epoch
    return $va[0] <=> $vb[0] unless($va[0] == $vb[0]);

    my $upstreamcmp = deb_verrevcmp($va[1], $vb[1]);
    return $upstreamcmp unless(defined $upstreamcmp and $upstreamcmp == 0);

    return deb_verrevcmp($va[2], $vb[2]);
}
sub deb_compare_nofail($$) {
    return  1 unless(deb_valid($_[0]));
    return -1 unless(deb_valid($_[1]));
    my @va = deb_parse($_[0]);
    my @vb = deb_parse($_[1]);

    # Epoch
    return $va[0] <=> $vb[0] unless($va[0] == $vb[0]);

    my $upstreamcmp = deb_verrevcmp($va[1], $vb[1]);
    return $upstreamcmp unless(defined $upstreamcmp and $upstreamcmp == 0);

    return deb_verrevcmp($va[2], $vb[2]);
}

1;
