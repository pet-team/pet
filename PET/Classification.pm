# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Module for classifying packages into problem clases. The idea is to make the
# reporting scripts absolutely minimal, and to have a common code in different
# report implementations.
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package PET::Classification;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(classify);

use PET::Cache;
#use PET::Common;
use PET::Config '%CFG';
use PET::DebVersions;

use Date::Parse;

# Takes a list of packages to process.
# Returns an unique hash ready to use in reporting, keyed by package name.
# package_name => {
#   status => {                 # Hash to ease lookup, empty if OK (@notes)
#       needs_upload => 1,
#       needs_upgrade => 1,
#       invalid_repo_version => 1,
#       ...
#   },
#   notes => [ ... ],
#   hilight => {                # Problems indexed by highlighted item
#       archive => { needs_upload => 1, ... },
#       bts => { has_bugs => 1 }, ...
#   },
#   repo_path => "...",
#   upstream_url => "...",      # Already extracted data for ease of use
#
#   bts => {},
#   archive => {},
#   svn => {},
#   watch => {}                 # Copies from the caches
# }

my %error_hilight = (
    archive_waiting => "archive",
    tagged_wait => "archive",
    needs_upload => "archive",
    never_uploaded => "archive",
    archive_foreign => "archive",
    waiting => "archive",
    has_rc_bugs => "bts",
    not_finished => "repo",
    not_finished_cruft => "repo",
    ignore_wip => "repo",
    repo_ancient => "repo",
    repo_foreign => "repo",
    invalid_tag => "repo",
    missing_tag => "repo",
    name_mismatch => "repo",
    needs_upgrade => "upstream",
    upstream_ancient => "upstream",
    watch_error => "upstream",
#    native => "",
);
my %warn_hilight = (
    has_bugs    => "bts",
    archive_nonmaint => "archive",
    repo_nonmaint => "repo",
    upgrade_in_progress => "repo",
);
# Arguments:
# classify([ { options } ], pkg1, pkg2,...)
# Where options is a hashref of configuration options for classify:
#  - ignore_forwarded_bugs: do not show nor count bugs marked as 'forwarded'.
#  - ignore_bug_keywords: arrayref of bug keywords. Bugs that cointain one of
#    those keywords (bug tags, usertags or the special
#    repo-(un)released:pending are not shown nor count.
#  - ignore_group_as_uploader: do not mark as a problem that the group is set
#    as uploader but not maintainer.
#
sub classify {
    my @pkglist = @_;
    my $data = read_cache(consolidated => "");
    my %res = ();

    my %args;
    if(@pkglist > 0 and ref $pkglist[0]) {
        if(ref $pkglist[0] eq 'HASH') {
            %args = %{$pkglist[0]};
            shift @pkglist;
        } else {
            die "Invalid call to classify()\n";
        }
    }
    my %opts;
    $opts{ignore_group_as_uploader} = delete $args{ignore_group_as_uploader};
    $opts{ignore_forwarded_bugs} = delete $args{ignore_forwarded_bugs};
    if($args{ignore_bug_keywords}) {
        unless(ref $args{ignore_bug_keywords}
                and ref $args{ignore_bug_keywords} eq 'ARRAY') {
            die "Invalid argument to ignore_bug_keywords\n";
        }
        $opts{ignore_bug_keywords} = delete $args{ignore_bug_keywords};
    }

    foreach my $pkg (@pkglist) {
        next if($pkg =~ /^\//);
        my(%status, @notes);
        # SVN versus archive
        my $archive_ver = $data->{archive}{$pkg}{most_recent};
        my $repo_ver = $data->{svn}{$pkg}{version};
        my $repo_unrel_ver = $data->{svn}{$pkg}{un_version};
        my $repo_unrel_date = $data->{svn}{$pkg}{un_date};
        my $tag_ver = $data->{svn}{$pkg}{tags}[-1];
        my $tag_ok = ($tag_ver and $repo_ver
                and not deb_compare($tag_ver, $repo_ver));
        if($tag_ver and $repo_ver and deb_compare($tag_ver, $repo_ver) > 0) {
            $status{invalid_tag} = 1;
        }
        if($tag_ver and $archive_ver
                and deb_compare($tag_ver, $archive_ver) < 0) {
            $status{missing_tag} = 1;
        }
        if(not $repo_ver or not $archive_ver) {
            if(not $repo_ver) {
                $status{not_finished} = 1;
		if($repo_unrel_date and $CFG{common}{never_uploaded_cruft_age})
		{
                    my $ts_chl = str2time($repo_unrel_date);
                    my $ts_now = time();
                    if($ts_now - $ts_chl > 60 * 60 * 24 *
			    $CFG{common}{never_uploaded_cruft_age}) {
                        $status{not_finished_cruft} = 1;
			push(@notes, "Cleanup candidate, last changelog date: ",
				$repo_unrel_date);
                    }
                }
            }
            if(not $archive_ver) {
                if($tag_ok) {
                    $status{tagged_wait} = 1;
                } else {
                    $status{never_uploaded} = 1;
                    # catch NEW packages that need to be uploaded
                    $status{needs_upload} = 1 if($repo_ver and
			    not $repo_unrel_ver);
                }
            }
        } elsif(deb_compare($archive_ver, $repo_ver) > 0) {
            $status{repo_ancient} = 1;
            push @notes, "$archive_ver > $repo_ver";
        } elsif(deb_compare($archive_ver, $repo_ver) != 0
                and not $repo_unrel_ver) {
            if($tag_ok) {
                $status{tagged_wait} = 1;
            } else {
                $status{needs_upload} = 1;
            }
        }
        if($pkg ne $data->{svn}{$pkg}{dir}) {
            $status{name_mismatch} = 1;
        }
        # SVN versus upstream
        my $repo_mangled_ver = $data->{svn}{$pkg}{mangled_ver};
        my $repo_unrel_mangled_ver = $data->{svn}{$pkg}{mangled_un_ver};
        my $upstream_mangled_ver = $data->{watch}{$pkg}{upstream_mangled};
        # watch_error from svn is not needed, as Watch.pm copies it
        my $watch_error = $data->{watch}{$pkg}{error};
        if($watch_error and $watch_error eq "Native") {
            #$status{native} = 1;
        } elsif($watch_error and $watch_error eq "Ignore") {
            #
        } elsif($watch_error) {
            $status{watch_error} = 1;
            push @notes, "Watch problem: $watch_error";
        } elsif((not $repo_mangled_ver and not $repo_unrel_mangled_ver)
                or not $upstream_mangled_ver) {
            $status{watch_error} = 1; # Should not happen
            push @notes, "Unexpected watchfile problem";
        } elsif($repo_mangled_ver) { # Will not check if UNRELEASED (?)
            if(deb_compare($repo_mangled_ver, $upstream_mangled_ver) > 0) {
                $status{upstream_ancient} = 1;
                push @notes, "$repo_mangled_ver > $upstream_mangled_ver";
            }
            if(deb_compare($repo_mangled_ver, $upstream_mangled_ver) < 0) {
                $status{needs_upgrade} = 1;
                $status{upgrade_in_progress} = 1 if(
                    deb_compare($repo_unrel_mangled_ver,
                        $upstream_mangled_ver) == 0);
            }
        }
        # Archive
        my $archive_latest = $data->{archive}{$pkg}{most_recent_src} || "";
        if($archive_latest =~ /^(new|incoming|deferred)$/) {
            $status{archive_waiting} = 1;
        }
        if($data->{bts}{$pkg} and %{$data->{bts}{$pkg}}) {
            foreach my $bug (keys %{$data->{bts}{$pkg}}) {
                if($opts{ignore_forwarded_bugs}
                        and $data->{bts}{$pkg}{$bug}{forwarded}) {
                    delete $data->{bts}{$pkg}{$bug};
                    next;
                }
                if($opts{ignore_bug_keywords}) {
                    my %ignore = map({ $_ => 1 } @{$opts{ignore_bug_keywords}});
                    if(grep({ $ignore{$_} } @{$data->{bts}{$bug}{keywordsA}})) {
                        delete $data->{bts}{$pkg}{$bug};
                        next;
                    }
                }
                # enumerating non-RC severities allows automatic support for
                # new RC severities
                $status{has_rc_bugs} = 1
                unless $data->{bts}{$pkg}{$bug}{severity} =~ m/
                    minor|wishlist|normal|important
                /x;

                next unless($data->{svn}{$pkg}{closes}{$bug});
                next if($data->{svn}{$pkg}{closes}{$bug} eq "released" and
                    not $status{needs_upload});
                # Values is released|unreleased.
                my $tag = "repo-".$data->{svn}{$pkg}{closes}{$bug}.":pending";
                push(@{$data->{bts}{$pkg}{$bug}{keywordsA}}, $tag);
                $data->{bts}{$pkg}{$bug}{keywords} .= " $tag";
            }
            if(%{$data->{bts}{$pkg}}) {
                $status{has_bugs} = 1;
            } else {
                delete $data->{bts}{$pkg};
            }
        }
        if($CFG{common}{group_email}
                and $data->{archive}{$pkg}{control}{maintainer}
                and $data->{archive}{$pkg}{control}{uploaders}
                and not grep( { /<\Q$CFG{common}{group_email}\E>/ }
                @{$data->{archive}{$pkg}{control}{maintainer}}
            )) {
            if(grep({ /<\Q$CFG{common}{group_email}\E>/ }
                    @{$data->{archive}{$pkg}{control}{uploaders}}
                )) {
                $status{archive_nonmaint} = 1
                unless($opts{ignore_group_as_uploader});
            } else {
                $status{archive_foreign} = 1;
            }
        }
        if($CFG{common}{group_email}
                and $data->{svn}{$pkg}{maintainer}
                and $data->{svn}{$pkg}{uploaders}
                and not grep( { /<\Q$CFG{common}{group_email}\E>/ }
                @{$data->{svn}{$pkg}{maintainer}}
            )) {
            if(grep({ /<\Q$CFG{common}{group_email}\E>/ }
                    @{$data->{svn}{$pkg}{uploaders}}
                )) {
                $status{repo_nonmaint} = 1
                unless($opts{ignore_group_as_uploader});
            } else {
                $status{repo_foreign} = 1;
            }
        }
        if(defined($data->{svn}{$pkg}{ignore_wip})) {
            $status{ignore_wip} = 1;
        }
        foreach my $waiting_package(keys %{$data->{svn}{$pkg}{waits_list}}) {
          my $waiting_version = $data->{svn}{$pkg}{waits_list}{$waiting_package};
          my $archive_version = @{[read_cache("archive", "unstable/$waiting_package", 0)]}[0]->{ver};
          if(!defined($archive_version)) {
            $status{waiting} += 1;
          } else {
            if(defined($waiting_version)) {
              unless(deb_compare($archive_version, $waiting_version) >= 0) {
                $status{waiting} += 1;
              }
            }
          }
        }
        my(%hilight, %warning);
        foreach(keys %status) {
            die "Internal error: $_ is not a valid status" unless(
                $error_hilight{$_} or $warn_hilight{$_});
            $hilight{$error_hilight{$_}}{$_} = 1 if($error_hilight{$_});
            $warning{$warn_hilight{$_}}{$_} = 1 if($warn_hilight{$_});
        }
        $res{$pkg} = {
            name    => $pkg,
            #
            watch   => $data->{watch}{$pkg},
            archive => $data->{archive}{$pkg},
            svn     => $data->{svn}{$pkg},
            bts     => $data->{bts}{$pkg},
            #
            repo_path => $data->{svn}{$pkg}{dir},
            upstream_url => $data->{watch}{$pkg}{upstream_url},
            #
            status  => \%status,
            notes   => \@notes,
            hilight => \%hilight,
            warning => \%warning
        };
    }
    return \%res;
}

1;
