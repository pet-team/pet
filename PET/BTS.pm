# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Module for retrieving bugs from the BTS, using the SOAP interface
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package PET::BTS;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(bts_download bts_get bts_get_consolidated);

use PET::Common;
use PET::Config '%CFG';
use PET::Cache;
use PET::Svn;
use SOAP::Lite;

#my $maint = 'pkg-perl-maintainers@lists.alioth.debian.org';

sub bts_download {
    my($force, @pkglist) = @_;
    $force ||= 0;
    debug("bts_download($force, (@pkglist))");

    my $replace = 0;

    my $soap = SOAP::Lite->uri($CFG{bts}{soap_uri})->proxy(
        $CFG{bts}{soap_proxy});

    my $cdata = read_cache("bts", "", 0);
    if(find_stamp($cdata, "") == 0) {
        warn("Forcing complete update -- bts cache has old version");
        $force = 1;
        @pkglist = ();
    }

    my @users = split(/\s*,\s*/, $CFG{bts}{usertag_users});
    my %usertags;
    if(@users) {
        if($force
                or $CFG{bts}{ttl} * 60 < time - find_stamp($cdata, "usertags")
                or grep({ ! $cdata->{usertags}{$_}} @users)) {
            info("Scanning usertags");
            foreach(@users) {
                $usertags{$_} = $soap->get_usertag($_)->result();
            }
        }
    }

    my @list = ();
    my $pkginfo = get_pkglist_hashref();
    if(@pkglist) {
        # A list of packages to update has been received
        unless($force) {
            @pkglist = grep( {
                    $CFG{bts}{ttl} * 60 < time - find_stamp($cdata, $_)
                } @pkglist);
        }
        if(@pkglist) {
            info("Downloading list of bugs for (", join(", ", @pkglist), ")");
            @list = @{$soap->get_bugs( src => [ @pkglist ] )->result()};
        }
    } elsif($force or $CFG{bts}{ttl} * 60 < time - find_stamp($cdata, "")) {
        # No list of packages; forced operation or stale cache
        info("BTS info is stale") unless($force);
        $replace = 1;
        @pkglist = keys %$pkginfo;
        # TODO: could verify that pkglist and maint = $maint are the same
        # packages
        if(@pkglist) {
            info("Downloading list of bugs of packages in the repo");
            @list = @{$soap->get_bugs( src => [ @pkglist ] )->result()};
        } else {
            # Doesn't make sense to search bugs if we don't have the list
            # of packages.
            warn("No packages to look bugs for yet");
            update_cache("bts", \%usertags, "usertags", 1, 0) if(%usertags);
            return {};
        }
    } # If cache is up-to-date, @list will be empty
    my $bugs_st = {};
    if(@list) {
        info("Downloading status for ", scalar @list, " bugs");
        $bugs_st = $soap->get_status(@list)->result();
    }

    my %bugs = ();
    foreach my $bug (keys %$bugs_st) {
        my $srcname = (defined $bugs_st->{$bug}->{source} &&
            $bugs_st->{$bug}->{source} ne "unknown" ?
            $bugs_st->{$bug}->{source} :
            $bugs_st->{$bug}->{package});
        if ($srcname) {
            my @srcs = split(/\s*,\s*/, $srcname);
            $bugs{$_}{$bug} = $bugs_st->{$bug} foreach(@srcs);
        } else {
            warn("BTS SOAP interface returned no source name for bug #$bug.");
        }
    }
    $bugs{usertags} = \%usertags if(%usertags);
    # retain lock, we need consistency
    $cdata = update_cache("bts", \%bugs, "", $replace, 1);
    bts_consolidate($cdata, keys %$pkginfo);
    unlock_cache("bts");
    return $cdata;
}
sub bts_consolidate {
    my($bugs, @pkglist) = @_;
    info("Re-generating consolidated hash");

    # Inverted index of usertags
    my %usertags;
    foreach my $user (keys %{$bugs->{usertags} || {}}) {
        next if($user =~ m#^/#);
        foreach my $tag (keys %{$bugs->{usertags}{$user} || {}}) {
            foreach(@{$bugs->{usertags}{$user}{$tag}}) {
                $usertags{$_} ||= [];
                push @{$usertags{$_}}, { user => $user, tag => $tag };
            }
        }
    }

    # TODO: Interesting fields:
    # keywords/tags, severity, subject, forwarded, date
    my %cbugs;
    foreach my $pkgname (@pkglist) {
        $bugs->{$pkgname} ||= {};

        # bugs to ignore if keyword present
        my %ign_keywords = map({ $_ => 1 }
            split(/\s*,\s*/, $CFG{bts}{ignore_keywords}));
        # bugs to ignore if of specified severities
        my %ign_severities = map({ $_ => 1 }
            split(/\s*,\s*/, $CFG{bts}{ignore_severities}));

        $cbugs{$pkgname} = {};
        foreach my $bug (keys %{ $bugs->{$pkgname} }) {
            next unless(ref $bugs->{$pkgname}{$bug});
            # Remove done bugs
            next if($bugs->{$pkgname}{$bug}{done});
            # Remove if severity match
            next if($ign_severities{$bugs->{$pkgname}{$bug}{severity}});
            # Remove if keyword match
            my @keywords = split(/\s+/, $bugs->{$pkgname}{$bug}{keywords});
            next if(grep({ $ign_keywords{$_} } @keywords));
            $cbugs{$pkgname}{$bug} = {
                keywords => $bugs->{$pkgname}{$bug}{keywords},
                # need to use a new key for compatibility
                keywordsA => \@keywords,
                severity => $bugs->{$pkgname}{$bug}{severity},
                subject  => $bugs->{$pkgname}{$bug}{subject},
                forwarded=> $bugs->{$pkgname}{$bug}{forwarded},
            };
            if($usertags{$bug}) {
                $cbugs{$pkgname}{$bug}{usertags} = $usertags{$bug};
                foreach(@{$usertags{$bug}}) {
                    $cbugs{$pkgname}{$bug}{keywords} .= " usertag:$_->{tag}";
                    push(@{$cbugs{$pkgname}{$bug}{keywordsA}},
                        "usertag:$_->{tag}");
                }
            }
        }
    }
    update_cache("consolidated", \%cbugs, "bts", 1, 0);
}
# Returns the hash of bugs. Doesn't download anything.
sub bts_get {
    return read_cache("bts", shift, 0);
}
# Returns the consolidated hash of bugs. Doesn't download anything.
sub bts_get_consolidated {
    my $path = shift || "";
    return read_cache("consolidated", "bts/$path", 0);
}
1;
