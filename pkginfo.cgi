#!/usr/bin/perl
# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Dump all info about a package
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2008
# © 2008 Damyan Ivanov <dmn@debian.org>
# Released under the terms of the GNU GPL 2
use strict;
use warnings;

use PET::Cache;
use PET::Config qw(read_config %CFG);

use CGI ':fatalsToBrowser';
use CGI ();
use Date::Parse ();
use File::Find ();
use List::Util qw(max);
use POSIX qw(locale_h);
use Template ();

read_config();

my $cgi = new CGI;

my $pkg = $cgi->param('pkg') or exit 0;
my $cache = read_cache(consolidated => "");

my @modified;
# Find recent template files
File::Find::find( { wanted => sub {
            my $mtime = (stat)[9];
            push @modified, $mtime if(-f _);
        } }, $CFG{pet_cgi}{templates_path} );
my $script_date = '$Date$';
push @modified, Date::Parse::str2time(
    join(' ', (split(/ /, $script_date))[1..3]));
push @modified, (stat $CFG{common}{cache_dir} . "/consolidated")[9];

my $last_modified = max @modified;

if($ENV{GATEWAY_INTERFACE}) {
    my $htmlp = $cgi->Accept("text/html");
    my $xhtmlp = $cgi->Accept("application/xhtml+xml");

    my $ims = $cgi->http('If-Modified-Since');
    $ims = Date::Parse::str2time($ims) if $ims;

    if($ims and $ims >= $last_modified) {
        print $cgi->header('text/html', '304 Not modified');
        exit 0;
    }

    my $old_locale = setlocale(LC_TIME);
    setlocale(LC_TIME, "C");
    print $cgi->header(
        -content_type   => (
                'text/plain; charset=utf-8'
#                ($xhtmlp and $xhtmlp > $htmlp)
#                ? 'application/xhtml+xml; charset=utf-8'
#                : 'text/html; charset=utf-8'
            ),
        -last_modified   => POSIX::strftime(
            "%a, %d %b %Y %T %Z",
            localtime($last_modified),
        )
    );
    setlocale(LC_TIME, $old_locale);
}


use Template;
my $tt = new Template(
    {
        INCLUDE_PATH => $CFG{pet_cgi}{templates_path},
        INTERPOLATE  => 1,
        POST_CHOMP   => 1,
        FILTERS      => {
            'quotemeta' => sub { quotemeta(shift) },
        },
        RECURSION   => 1,
    }
);

$tt->process(
    'pkginfo',
    {
        sections    => [
            {
                title   => 'SVN',
                data    => $cache->{svn}{$pkg},
            },
            {
                title   => 'Upstream',
                data    => $cache->{watch}{$pkg},
            },
            {
                title   => 'Archive',
                data    => $cache->{archive}{$pkg},
            },
            {
                title   => 'Bugs',
                data    => $cache->{bts}{$pkg},
            },
        ],
    },
) || die $tt->error();
exit 0;

