# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Svn.pm 610 2008-08-30 06:50:16Z tincho $
#
# Module for SVN access
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2009
# Released under the terms of the GNU GPL 2

package PET::RepoAccess::SVN;
use strict;
use warnings;

use Carp;
use IO::Scalar;
use SVN::Client;

our @ISA = qw(PET::RepoAccess);

=pod

=encoding utf-8

=head1 NAME

PET::RepoAccess::SVN - Interface for low-level access to SVN repositories

=head1 SYNOPSIS

    use PET::RepoAccess;
    
    $repo = new PET::RepoAccess(SVN => "file:///opt/myrepo");

=head1 DESCRIPTION

PET::RepoAccess provides an abstraction for accessing different kinds of
version control systems under an uniform API.

This module provides the SVN-specific fuctions.

=head1 SEE ALSO

L<PET::RepoAccess(3)>

=head1 COPYRIGHT

Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2009

=head1 LICENSE

Released under the terms of the GNU GPL 2

=cut
sub new {
    my $type = shift;
    $type = ref $type || $type;
    if(@_ % 2) {
        unshift @_, "repo";
    }
    my %param = @_;
    unless($param{repo}) {
        croak "Invalid call to ${type}::new";
    }
    my $obj = {};
    $obj->{repo} = _svn_sanitize_uri(delete $param{repo});
    $obj->{c} = SVN::Client->new();

    bless($obj, $type);
    return $obj;
}
sub clone {
    my $self = shift;
    return $self->new(repo => $self->{repo});
}
# $foo->list("/path", 1) = \@contents
# undef if path doesn't exist
sub list {
    my $self = shift;
    return $self->_ls(undef, @_);
}
sub listdirs {
    my $self = shift;
    return $self->_ls($SVN::Node::dir, @_);
}
sub listfiles {
    my $self = shift;
    return $self->_ls($SVN::Node::file, @_);
}
sub stat {
    my $self = shift;
    my $path = shift;
    my $stat;
    _svn_safe_op($self->{c},
        info => _svn_sanitize_uri($self->{repo} . "/" . $path),
        'HEAD', 'HEAD', sub { $stat = $_[1] }, 0) or return undef;
    my %ret;
    if($stat->kind == $SVN::Node::file) {
        $ret{type} = "file";
    } elsif($stat->kind == $SVN::Node::dir) {
        $ret{type} = "dir";
    } else {
        $ret{type} = "unknown";
    }
    $ret{mtime} = $stat->last_changed_date / 1000000;
    $ret{mrev} = $stat->last_changed_rev;
    $ret{mauth} = $stat->last_changed_author;
    return \%ret;
}
sub get_file {
    my($self, $path) = @_;
    my $data;
    my $fh = IO::Scalar->new(\$data);
    my $r = _svn_safe_op($self->{c}, cat => $fh,
        _svn_sanitize_uri($self->{repo} . "/" . $path), "HEAD");
    $fh->close();
    $data = "" unless(defined $data);
    return $r ? $data : undef;
}
sub get_changes {
    my($self, $path, $rev) = @_;
    my %changes;
    $path = _svn_sanitize_uri($self->{repo} . "/" . $path);
    my $subpath = substr($path, length($self->{repo}));
    _svn_safe_op($self->{c}, log => [ $path ],
        $rev, "HEAD", 1, 1, sub {
            foreach (keys %{$_[0]}) {
                $changes{$1} = 1 if(m{^\Q$subpath\E/(.*?)$});
            }
        }) or return undef;
    return [ keys %changes ];
}
#
# Private methods
sub _ls {
    my $self = shift;
    my $filt = shift;
    my $path = shift;
    my $recurse = shift || 0;
    my $ret = _svn_safe_op($self->{c},
        ls => _svn_sanitize_uri($self->{repo} . "/" . $path),
        'HEAD', $recurse);
    return undef unless(ref $ret);
    return keys %$ret unless $filt;
    my @ret = grep( { $ret->{$_}->kind() == $filt } keys %$ret);
    return \@ret;
}
sub _svn_safe_op($$@) {
    my($svn, $op, @opts) = @_;
    local $SVN::Error::handler = undef;
    my ($svn_out) = eval "\$svn->$op(\@opts)";
    if(SVN::Error::is_error($svn_out)) {
        my $msg = $svn_out->message();
        my $cod = $svn_out->apr_err();
        my $err = $svn_out->strerror();

        if($cod == $SVN::Error::FS_NOT_FOUND or
            # loathe stupid SVN api
            $cod == $SVN::Error::RA_ILLEGAL_URL && $op eq "info") {
            $svn_out->clear();
            return 0;
        } else {
            confess("Error $cod ($err) from SVN: $msg");
        }
    }
    return $svn_out || "0E0";
}
# Removes duplicate slashes, slashes at the end and adds file:// protocol if
# needed.
sub _svn_sanitize_uri {
    my $uri = shift;
    if($uri =~ m#^/# and $uri !~ m#://#) {
        $uri = "file://" . $uri;
    }
    $uri =~ m{^([^/:]+)://([^/]*)/(.*)$}
        or croak "Invalid SVN repository: $uri";
    my($repoproto, $repohost, $repopath) = ($1, $2, $3);
    $repopath =~ s#/+#/#g;
    $repopath =~ s{/$}{};
    $repopath =~ s{^/}{};
    return "$repoproto://$repohost/$repopath";
}

1;
