# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Routines for handling cache files 
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package PET::Cache;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = (qw(
    dump_cache unlock_cache read_cache update_cache find_stamp ));

use PET::Config '%CFG';
use PET::Common;
use Storable qw(store_fd fd_retrieve);
use Fcntl qw(:seek :flock);
use File::Path;

my %fd;         # Hash of open FDs, to keep locks.
my %memcache;   # Memory cache for repeated requests

sub dump_cache($;$);
sub unlock_cache($);
sub read_cache($;$$);
sub update_cache($$;$$$$);
sub clean_hash($);
sub clean_hash_recurse($);
sub dive_hash($;$);
sub find_stamp($;$);
sub find_stamp_recurse($$);

sub dump_cache($;$) {
    my($cache, $root) = @_;
    $root ||= "";
    $root =~ s{/+$}{};

    if(! defined($fd{$cache})) {
        mkpath $CFG{common}{cache_dir};
        open $fd{$cache}, "<", "$CFG{common}{cache_dir}/$cache"
            or die "Error opening cache: $!\n";
        flock($fd{$cache}, LOCK_SH) or die "Error locking cache: $!\n";
    }
    my $fd = $fd{$cache};
    seek($fd, 0, SEEK_SET) or die "Can't seek: $!\n";
    my $data = {};
    if(-s $fd) {
        $data = fd_retrieve($fd) or die "Can't read cache: $!\n";
    }
    unlock_cache($cache);
    require Data::Dumper;
    print Data::Dumper::Dumper(dive_hash($data, $root));
    1;
}
# Releases any pending lock on a cache and closes the file.
sub unlock_cache($) {
    my $cache = shift;
    return 0 unless($fd{$cache});
    debug("Closing $CFG{common}{cache_dir}/$cache");
    close($fd{$cache});
    $fd{$cache} = undef;
    1;
}
sub read_cache($;$$) {
    # * $root specifies a path inside the cache hash.
    # * If $keep_lock, file is kept open and write-locked until the next
    #   operation.
    #
    # In scalar context returns the data as a hashref. In array context also
    # returns the effective stamp as a second element. The effective
    # stamp is the value of a "/stamp" key at the same level (or up) as
    # $root. If there are single elements with newer stamps, they will have
    # a "/stamp" subkey.
    my($cache, $root, $keep_lock) = @_;
    $root ||= "";
    $keep_lock ||= 0;
    debug("read_cache($cache, $root, $keep_lock) invoked");

    $root = "/$root";
    $root =~ s{/+$}{};
    
    my $file = "$CFG{common}{cache_dir}/$cache";
    unless(-e $file) {
        return({}, 0) if(wantarray);
        return {};
    }
    my $use_memcache = 0;
    if(! defined($fd{$cache})) {
        mkpath $CFG{common}{cache_dir};
        if($keep_lock) {
            debug("Opening $file in RW mode");
            open $fd{$cache}, "+<", $file or die "Error opening cache: $!\n";
            flock($fd{$cache}, LOCK_EX) or die "Error locking cache: $!\n";
        } else {
            if($memcache{$cache} and $memcache{$cache}{mtime} == -M $file) {
                $use_memcache = 1;
            } else {
                debug("Opening $file in R mode");
                open $fd{$cache}, "<", $file or die "Error opening cache: $!\n";
                flock($fd{$cache}, LOCK_SH) or die "Error locking cache: $!\n";
            }
        }
    }
    my $data = {};
    if($use_memcache) {
        $data = $memcache{$cache}{data};
    } else {
        my $fd = $fd{$cache};
        seek($fd, 0, SEEK_SET) or die "Can't seek: $!\n";
        if(-s $fd) {
            $data = fd_retrieve($fd) or die "Can't read cache: $!\n";
        }
        unlock_cache($cache) unless($keep_lock);
        $memcache{$cache} = {
            data => $data,
            mtime => -M $file
        };
    }
    my $rootd = dive_hash($data, $root);
    return $rootd if(not wantarray);
    return($rootd, find_stamp($data, $root));
}
sub update_cache($$;$$$$) {
    # $cache, $data, $root, $replace, $keep_lock, $stamp
    # * $data is the data to merge/replace (depending on $replace) in the cache
    #   starting from $root. Note that it's merged at the first level: so
    #   existant data inside a key won't be kept.
    # * $root specifies a path inside the cache hash.
    # * If $keep_lock, file is kept open and write-locked until the next
    #   operation.
    #
    # A $stamp is added with key "/stamp", at the $root level if $replace,
    # inside each key if not. If no $stamp is specified, the current unix time
    # is used.
    #
    # Returns the whole cache
    my($cache, $data, $root, $replace, $keep_lock, $stamp) = @_;
    $root ||= "";
    $root = "/$root";
    $root =~ s{/+$}{};
    $replace ||= 0;
    $keep_lock ||= 0;
    $stamp = time unless(defined $stamp);
    debug("update_cache($cache, $data, $root, $replace, $keep_lock, $stamp) ",
        "invoked");

    my $file = "$CFG{common}{cache_dir}/$cache";
    if(! defined($fd{$cache})) {
        debug("Opening $file in RW mode");
        if(-e $file) {
            open($fd{$cache}, "+<", $file) or die "Error opening cache: $!\n";
        } else {
            mkpath $CFG{common}{cache_dir};
            open($fd{$cache}, "+>", $file) or die "Error opening cache: $!\n";
        }
        flock($fd{$cache}, LOCK_EX) or die "Error locking cache: $!\n";
    }
    my $fd = $fd{$cache};
    seek($fd, 0, SEEK_SET) or die "Can't seek: $!\n";
    my $cdata = {};
    if(-s $fd) {
        $cdata = fd_retrieve($fd) or die "Can't read cache: $!\n";
    }
    if($replace) {
        if($root =~ m{^/*$}) {
            $root = $cdata = $data;
        } else {
            $root =~ s{/+([^/]+)$}{};
            my $leaf = $1;
            $root = dive_hash($cdata, $root);
            $root = ($root->{$leaf} = $data);
        }
        clean_hash($root);
        $root->{"/stamp"} = $stamp;
        $root->{"/version"} = $VERSION;
    } else {
        $root = dive_hash($cdata, $root);
        foreach(keys(%$data)) {
            $root->{$_} = $data->{$_};
            $root->{$_}{"/stamp"} = $stamp;
        }
    }
    seek($fd, 0, SEEK_SET) or die "Can't seek: $!\n";
    truncate($fd, 0) or die "Can't truncate: $!\n";
    store_fd($cdata, $fd) or die "Can't save cache: $!\n";
    unless($keep_lock) {
        unlock_cache($cache);
        $memcache{$cache} = {
            data => $cdata,
            mtime => -M $file
        };
    }
    return $cdata;
}
# Deep-greps a hash looking for "magic" keys and removes them
sub clean_hash($) {
    my($hash) = @_;
    debug("clean_hash($hash) invoked");
    die "Invalid hashref" unless($hash and ref $hash and ref $hash eq "HASH");
    clean_hash_recurse($hash);
}
sub clean_hash_recurse($) {
    my($hash) = @_;
    foreach(keys %$hash) {
        delete $hash->{$_} if(m#^/#);
    }
    foreach(values %$hash) {
        clean_hash_recurse($_) if($_ and ref $_ and ref $_ eq "HASH");
    }
}
# Return a reference into $hash, as specified with $path
# Creates or replaces any component that is not a hashref
sub dive_hash($;$) {
    my($hash, $path) = @_;
    $path ||= "";
    debug("dive_hash($hash, $path) invoked");
    die "Invalid hashref" unless($hash and ref $hash and ref $hash eq "HASH");
    my @path = split(m#/+#, $path);
    my $ref = $hash;
    foreach(@path) {
        next unless($_);
        my $r = $ref->{$_};
        unless($r and ref $r and ref $r eq "HASH") {
            $r = $ref->{$_} = {};
        }
        $ref = $r;
    }
    return $ref;
}
# Search a stamp in $hash, starting at $path and going upwards until the
# root. Returns 0 if not found.
# Remember to call it with the root of the cache, to have proper stamp and
# version handling.
sub find_stamp($;$) {
    my($hash, $path) = @_;
    $path ||= "";
    debug("find_stamp($hash, $path) invoked");
    die "Invalid hashref" unless($hash and ref $hash and ref $hash eq "HASH");
    if(not $path and (not $hash->{"/version"} or
            $hash->{"/version"} < $VERSION)) {
        info("find_stamp: returning 0 as cache has old version");
        return 0;
    }
    return find_stamp_recurse($hash, $path);
}
sub find_stamp_recurse($$) {
    my($hash, $path) = @_;
    my $ctsmp = 0;
    if($path =~ s{^/*([^/]+)}{}) {
        my $root = $1;
        $ctsmp = find_stamp_recurse($hash->{$root}, $path) if($hash->{$root});
    }
    if(not $ctsmp and exists($hash->{"/stamp"})) {
        $ctsmp = $hash->{"/stamp"};
    }
    return $ctsmp || 0;
}
1;
