#!/usr/bin/perl
# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Report packages version states
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Released under the terms of the GNU GPL 2
use strict;
use warnings;

use PET::Cache;
use PET::Classification;
use PET::Config qw(read_config %CFG);
use PET::Svn;
use CGI ();
use CGI::Carp qw(fatalsToBrowser);
use POSIX qw(locale_h);
use Template ();
use Date::Parse ();
use File::Find ();
use List::Util qw(max);

read_config();

my $cgi = new CGI;

my %param_defaults = (
    template =>         $CFG{pet_cgi}{default_template},
    show_all =>         $CFG{pet_cgi}{default_show_all},
    start_collapsed =>  $CFG{pet_cgi}{default_start_collapsed},
    hide_binaries =>    $CFG{pet_cgi}{default_hide_binaries},
    refresh =>          $CFG{pet_cgi}{default_refresh},
    format =>           $CFG{pet_cgi}{default_format},
    ignore_keywords =>  $CFG{pet_cgi}{default_ignore_keywords},
    ignore_forwarded => $CFG{pet_cgi}{default_ignore_forwarded},
);
foreach(qw(show_all start_collapsed hide_binaries)) {
    $param_defaults{$_} = $param_defaults{$_} ? 1 : 0;
}
$param_defaults{ignore_keywords} = [ split(
    /\s*,\s*/, ($param_defaults{ignore_keywords} || "")) ];

my %params;
foreach(qw(template show_all start_collapsed hide_binaries refresh format
    ignore_keywords ignore_forwarded)) {
    my @p = $cgi->param($_);
    if(@p) {
        $params{$_} = ref $param_defaults{$_} ? \@p : $p[0];
    } else {
        $params{$_} = $param_defaults{$_};
    }
}

my $cache = read_cache(consolidated => "");

my @modified;
# Find recent template files
File::Find::find( { wanted => sub {
            my $mtime = (stat)[9];
            push @modified, $mtime if(-f _);
        } }, $CFG{pet_cgi}{templates_path} );
my $script_date = '$Date$';
push @modified, Date::Parse::str2time(
    join(' ', (split(/ /, $script_date))[1..3]));
push @modified, (stat $CFG{common}{cache_dir} . "/consolidated")[9];

my $last_modified = max @modified;
my @pkglist = get_pkglist();
my $cls = classify({
        ignore_bug_keywords => $params{ignore_keywords},
        ignore_forwarded_bugs => $params{ignore_forwarded},
        ignore_group_as_uploader => $CFG{pet_cgi}{ignore_group_as_uploader},
    }, @pkglist);

my( @no_prob, @for_upload, @for_upgrade, @upgrade_wip, @weird, @incoming,
    @itp_wip, @wip, @with_rc_bugs, @with_bugs, @tagged, @all, @ignore_wip,
    @waiting );

# List of seen bug keywords, to offer ignore list
my %keywords = ();
foreach my $p (values %$cls) {
    foreach my $bug (keys %{$p->{bts}}) {
        $keywords{$_} = 1 foreach(@{$p->{bts}{$bug}{keywordsA}});
    }
}

unless($params{show_all})
{
    foreach(keys %$cls)
    {
        delete $cls->{$_} unless(%{$cls->{$_}{hilight}} or
            %{$cls->{$_}{warning}});
    }
}

foreach my $pkg (sort keys %$cls)
{
    my $data = $cls->{$pkg};

    my $dest;   # like "destiny" :)
    my $status = $data->{status};   # to save some typing

    unless(($status->{needs_upgrade} and not $status->{upgrade_in_progress}) or $status->{has_rc_bugs}) {
        $dest = \@waiting if($status->{waiting});
    }
    $dest ||= \@tagged if($status->{tagged_wait});
    $dest ||= \@for_upload if($status->{needs_upload});
    $dest ||= \@incoming if $status->{archive_waiting};
    $dest ||= \@with_rc_bugs if $status->{has_rc_bugs};
    $dest ||= \@upgrade_wip if($status->{upgrade_in_progress} and
        not $status->{ignore_wip});
    $dest ||= \@for_upgrade if($status->{needs_upgrade} and
        not $status->{upgrade_in_progress});
    $dest ||= \@wip if($status->{name_mismatch});
    $dest ||= \@itp_wip if($status->{never_uploaded});
    $dest ||= \@wip if($status->{invalid_repo_version} or $status->{invalid_tag}
            or $status->{missing_tag});
    $dest ||= \@wip if($status->{not_finished} && !$status->{ignore_wip});
    $dest ||= \@weird if($status->{repo_ancient} or $status->{repo_ancient} or
        $status->{upstream_ancient});
    $dest ||= \@wip if $status->{watch_error};
    $dest ||= \@with_bugs if $status->{has_bugs};
    $dest ||= \@wip if $status->{archive_foreign};
    # $dest ||= \@wip if $status->{repo_foreign};
    $dest ||= \@ignore_wip if($status->{ignore_wip});
    $dest ||= \@no_prob;

    push @$dest, $data;
    push @all, $data;
}

if( $ENV{GATEWAY_INTERFACE} )
{
    my $htmlp = $cgi->Accept("text/html");
    my $xhtmlp = $cgi->Accept("application/xhtml+xml");

    my $ims = $cgi->http('If-Modified-Since');
    $ims = Date::Parse::str2time($ims) if $ims;

    if( $ims and $ims >= $last_modified )
    {
        print $cgi->header('text/html', '304 Not modified');
        exit 0;
    }

    my $old_locale = setlocale(LC_TIME);
    setlocale(LC_TIME, "C");
    print $cgi->header(
        -content_type   => (
                ($xhtmlp and $xhtmlp > $htmlp)
                ? 'application/xhtml+xml; charset=utf-8'
                : 'text/html; charset=utf-8'
            ),
        -last_modified   => POSIX::strftime(
            "%a, %d %b %Y %T %Z",
            localtime($last_modified),
        ),
        $params{"refresh"} ? (-refresh => $params{"refresh"}) : (),
    );
    setlocale(LC_TIME, $old_locale);
}

my $tt = new Template(
    {
        INCLUDE_PATH => $CFG{pet_cgi}{templates_path},
        INTERPOLATE  => 1,
        POST_CHOMP   => 1,
        FILTERS      => {
            'quotemeta' => sub { quotemeta(shift) },
        },
    }
);

$tt->process(
    $params{"template"},
    {
        data        => $cls,
        group_email => $CFG{common}{group_email},
        group_name  => $CFG{pet_cgi}{group_name},
        group_url   => $CFG{pet_cgi}{group_url},
        scm_web_file=> $CFG{pet_cgi}{scm_web_file},
        scm_web_dir => $CFG{pet_cgi}{scm_web_dir},
        (
            ( ($params{'format'}||'') eq 'list' )
            ? (
                all => \@all
            )
            : (
                all         => \@no_prob,
                for_upgrade => \@for_upgrade,
                upgrade_wip => \@upgrade_wip,
                weird       => \@weird,
                for_upload  => \@for_upload,
                incoming    => \@incoming,
                tagged      => \@tagged,
                itp_wip     => \@itp_wip,
                wip         => \@wip,
                with_bugs   => \@with_bugs,
                with_rc_bugs=> \@with_rc_bugs,
                ignore_wip  => \@ignore_wip,
                waiting     => \@waiting,
            )
        ),
        shown_packages => scalar(@all),
        total_packages => scalar(@pkglist),
        last_modified  => POSIX::strftime("%a, %d %b %Y %T %Z",
            localtime($last_modified)),
        now            => POSIX::strftime("%a, %d %b %Y %T %Z",
            localtime(time)),
        keywords       => [ "", sort keys %keywords ],
        param_defaults => \%param_defaults,
        params         => \%params,
    },
) || die $tt->error;

exit 0;

