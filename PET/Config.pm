# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Module that holds configuration variables. Also has subroutines for parsing
# command line options and the configuration file.
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2
package PET::Config;

use strict;
use warnings;

use FindBin;
use Getopt::Long;

our @EXPORT = qw(%CFG read_config getopt_common);
our @ISA = "Exporter";

# Default values
my %defaults = (
    pet_cgi => {
        templates_path              => "templates",
        default_template            => "by_category",
        group_name                  => "Unnamed Packaging Group",
        group_url                   => "http://www.debian.org/",
        scm_web_dir                 => undef,
        scm_web_file                => undef,
        # deprecated
          viewsvn_url               => undef,
          wsvn_url                  => undef,
        ignore_group_as_uploader    => 0,
        default_show_all            => 0,
        default_start_collapsed     => 0,
        default_hide_binaries       => 0,
        default_refresh             => 1800,        # 30 minutes
        default_format              => "categories",# or list
        default_ignore_keywords     => undef,       # comma-separated list
        default_ignore_forwarded    => 0
    },
    repository => {
        uri                         => undef,
        type                        => "SVN",
        packages_path               => "trunk",
        post_path                   => "",
        track_tags                  => 0,
        tags_path                   => "tags",
        tags_post_path              => ""
    },
    archive => {
        mirror          => "ftp://ftp.debian.org/debian",
        suites          => "unstable, testing, stable, oldstable, experimental",
        sections        => "main, contrib, non-free",
        suites_ttl      => "360, 360, 10080, 10080, 360",
        new_url         => 'http://ftp-master.debian.org/new.822',
        new_ttl         => 60,
        incoming_url    => 'http://incoming.debian.org/',
        incoming_ttl    => 60,
        deferred_url    => 'http://ftp-master.debian.org/deferred/status',
        deferred_ttl    => 60,
    },
    watch => {
        ttl             => 360,
        use_cpan        => 1,
        cpan_mirror     => "ftp://cpan.org/ls-lR.gz",
        cpan_ttl        => 360 # 6 hours
    },
    bts => {
        ttl                 => 60, # 1 hour
        soap_proxy          => 'http://bugs.debian.org/cgi-bin/soap.cgi',
        soap_uri            => 'Debbugs/SOAP',
        usertag_users       => 'debian-qa@lists.debian.org',
        ignore_keywords     => "",
        ignore_severities   => ""
    },
    common => {
        cache_dir                   => "~/.pet/cache",
        # verbosity level: error => 0, warn => 1, info => 2, debug => 3
        # Should be 1 by default, 0 for quiet mode
        verbose                     => 1,
        # Prepend syslog-style format?
        formatted_log               => 1,
        group_email                 => undef,
        never_uploaded_cruft_age    => 90,
    }
);
my %repeatable = (
    repository => 1
);
our %CFG = %defaults; # Global configuration
my %valid_cfg;
foreach my $section (keys %defaults) {
    $valid_cfg{$section} = { map({ $_ => 1 } keys(%{$defaults{$section}})) };
}
# Deprecated
$valid_cfg{svn} = {
    repository => 1,
    packages_path => 1,
    post_path => 1,
    track_tags => 1,
    tags_path => 1,
    tags_post_path => 1
};

sub read_config(;$) {
    my $file = shift;
    unless($file) {
        if($ENV{PET_CONF}) {
            $file = $ENV{PET_CONF};
        } elsif($ENV{HOME} and -e "$ENV{HOME}/.pet/pet.conf") {
            $file = "$ENV{HOME}/.pet/pet.conf";
        } elsif(-e "/etc/pet.conf") {
            $file = "/etc/pet.conf";
        } elsif(-e "pet.conf") {
            $file = "pet.conf";
        } elsif(-e "$FindBin::Bin/pet.conf") {
            $file = "$FindBin::Bin/pet.conf";
        } else {
            die "Can't find any configuration file!\n";
        }
    }
    die "Can't read configuration file: $file\n" unless(-r $file);

    my $section = "common";
    my $cfgdst = $CFG{common};
    open(CFG, "<", $file) or die "Can't open $file: $!\n";
    while(<CFG>) {
        chomp;
        s/(?<!\S)[;#].*//;
        s/\s+$//;
        next unless($_);
        if(/^\s*\[\s*(\w+)\s*\]\s*$/) {
            $section = lc($1);
            if($section =~ m#(.*)/(.+)#) {
                my $subsection = $2;
                $section = $1;
                unless($repeatable{$section}) {
                    die "Section $section is not repeatable\n";
                }
                unless($subsection =~ /^\w+$/) {
                    die "Invalid name for subsection: $subsection\n";
                }
                $CFG{$section}{$subsection} ||= {};
                $cfgdst = $CFG{$section}{$subsection};
                $cfgdst->{name} = $subsection;
            } else {
                $CFG{$section} ||= {};
                $cfgdst = $CFG{$section};
            }
            die "Invalid section in configuration file: $section\n" unless(
                exists($valid_cfg{$section}));
            next;
        }
        unless(/^\s*([^=]+?)\s*=\s*(.*)/) {
            die "Unrecognised line in configuration file: $_\n";
        }
        my($key, $val) = ($1, $2);
        unless(exists($valid_cfg{$section}{$key})) {
            die("Unrecognised configuration parameter $key in section " .
                "$section\n");
        }
        if($val =~ s/^~\///) { # UGLY!
            die "Can't use ~/ paths if \$HOME is not set!\n" unless($ENV{HOME});
            $val = $ENV{HOME} . "/$val";
        }
        $cfgdst->{$key} = $val;
    }
    close CFG;

    # Backwards compatibility for svn section
    if($CFG{svn}) {
        $CFG{repository} = delete $CFG{svn};
        $CFG{repository}{uri} = delete $CFG{repository}{repository};
        $CFG{repository}{type} = "SVN";
    }
    ## provide backward-compatibility with instances that still use (w|view)svn
    # file
    $CFG{pet_cgi}{scm_web_file} ||= 
        $CFG{pet_cgi}{viewsvn_url}.'?view=markup'
        if $CFG{pet_cgi}{viewsvn_url};
    $CFG{pet_cgi}{scm_web_file} ||= 
        $CFG{pet_cgi}{wsvn_url}.'/?op=file&rev=0&sc='
        if $CFG{pet_cgi}{wsvn_url};
    # dir
    $CFG{pet_cgi}{scm_web_dir} ||= 
        $CFG{pet_cgi}{viewsvn_url}.'/?'
        if $CFG{pet_cgi}{viewsvn_url};
    $CFG{pet_cgi}{scm_web_dir} ||= 
        $CFG{pet_cgi}{wsvn_url}.'/?rev=0&sc='
        if $CFG{pet_cgi}{wsvn_url};
    $CFG{pet_cgi}{scm_web_file} =~ s'%s'${pkg}/${file}';
    $CFG{pet_cgi}{scm_web_dir} =~ s'%s'${pkg}/${dir}';
}
# Parses command line options, loads configuration file if specified, removes
# arguments from @ARGV and returns a hash with the parsed options.
# If $passthru, ignores unknown parameters and keeps them in @ARGV.
# If $readconf, will call read_config even if the user didn't say --conf
sub getopt_common(;$$) {
    my($passthru, $readconf) = @_;
    my($conffile, $force, $v, $q) = (undef, 0, 0, 0);
    my $p = new Getopt::Long::Parser;
    $p->configure(qw(no_ignore_case bundling),
        $passthru ? ("pass_through") : ());
    $p->getoptions(
        'conf|c=s' => \$conffile, 'force|f!' => \$force,
        'verbose|v:+' => \$v, 'quiet|q:+' => \$q
    ) or die("Error parsing command-line arguments\n");
    read_config($conffile) if($conffile or $readconf);
    $CFG{common}{verbose} += $v - $q;
    return {
        force => $force     # only one argument for now
    };
}
1;
