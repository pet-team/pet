# vim:ts=4:sw=4:et:ai:sts=4
# $Id: Svn.pm 610 2008-08-30 06:50:16Z tincho $
#
# Abstraction to access repositories
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2009
# Released under the terms of the GNU GPL 2

package PET::RepoAccess;
use strict;
use warnings;

use Carp;

=pod

=encoding utf-8

=head1 NAME

PET::RepoAccess - Interface for low-level access to repositories

=head1 SYNOPSIS

    use PET::RepoAccess;
    
    $repo = new PET::RepoAccess(SVN => "file:///opt/myrepo");

=head1 DESCRIPTION

PET::RepoAccess provides an abstraction for accessing different kinds of
version control systems under an uniform API.

=head2 Constructor

=over 4

=item new(I<type>, I<options>)

=item PET::RepoAccess::I<type>->new(I<options>)

Constructs a new subclassed object for the specified repository type.
I<options> is a list of key-value named arguments. Only B<repo> is a valid
option for now.

=back

=head2 Methods

=over 4

=item list(I<path>, I<recurse>)

=item listfiles(I<path>, I<recurse>)

=item listdirs(I<path>, I<recurse>)

Retrieves a list of files and/or sub-directories in I<path>. If I<recurse> is a
true value, it will descend into sub-direcories.

Returns an array reference or undef if I<path> doesn't exist.

=item stat(I<path>)

Obtains stat-like information for I<path>.
Returns a hash reference or undef if I<path> doesn't exist.
The returned fields are:

=over 8

=item type

C<file>, C<dir> or C<unknown>

=item mtime

A unix timestamp for the last saved change.

=item mrev

A repository-specific indentifier (revision or equivalent) for the last saved
change.

=item mauth

The author of the last saved change.

=back

=item get_file(I<path>)

Obtains a file from the repository and returns it as a scalar. If the file
doesn't exist or there is a problem, returns undef.

=item get_changes(I<path>, I<rev>)

Obtains a list of all changed files under I<path>, since the point in time
defined by I<rev>. Returns an array reference or undef if there was a problem.

=back

=head1 SEE ALSO

L<PET::RepoAccess::SVN(3)>,
L<PET::RepoAccess::GIT(3)>,
etc

=head1 COPYRIGHT

Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2009

=head1 LICENSE

Released under the terms of the GNU GPL 2

=cut
sub new {
    my $class = shift;
    $class = ref $class || $class;
    my $type = shift;
    croak "Invalid repository type: $type" unless(
        $type =~ /^\w+$/ and eval "require PET::RepoAccess::$type");
    return "PET::RepoAccess::$type"->new(@_);
}

1;
