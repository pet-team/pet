#!/usr/bin/perl
# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# RSS feed for packages that need upload
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2008
# Released under the terms of the GNU GPL 2

use strict;
use warnings;

use PET::Cache;
use PET::Classification;
use PET::Config qw(read_config %CFG);
use PET::Svn;
use CGI ();
use CGI::Carp qw(fatalsToBrowser);
use POSIX qw(locale_h);
use Template ();
use Date::Parse ();
use List::Util qw(max);
use XML::RSS;
use Encode;

read_config();

my $cgi = new CGI;

my $cache = read_cache(consolidated => "");

my @modified;
my $script_date = '$Date$';
push @modified, Date::Parse::str2time(
    join(' ', (split(/ /, $script_date))[1..3]));
push @modified, (stat $CFG{common}{cache_dir} . "/consolidated")[9];

my $last_modified = max @modified;
my @pkglist = get_pkglist();
my $cls = classify(@pkglist);
my @for_upload;

foreach my $pkg (sort keys %$cls)
{
    my $data = $cls->{$pkg};

    my $dest;   # like "destiny" :)
    my $status = $data->{status};   # to save some typing

    $dest ||= \@for_upload if($status->{needs_upload});

    push @$dest, $data;
}

if( $ENV{GATEWAY_INTERFACE} )
{
    my $textxmlp = $cgi->Accept("text/xml");
    my $appxmlp= $cgi->Accept("application/xml");

    my $ims = $cgi->http('If-Modified-Since');
    $ims = Date::Parse::str2time($ims) if $ims;

    if( $ims and $ims >= $last_modified )
    {
        print $cgi->header('text/html', '304 Not modified');
        exit 0;
    }

    my $old_locale = setlocale(LC_TIME, "C");
    print $cgi->header(
        -content_type   => (
                ($appxmlp and $appxmlp >= $textxmlp)
                ? 'application/rss+xml; charset=utf-8'
                : 'text/xml; charset=utf-8'
            ),
        -last_modified   => POSIX::strftime(
            "%a, %d %b %Y %T %Z",
            localtime($last_modified),
        ),
        $cgi->param("refresh") ? (-refresh => $cgi->param("refresh")) : (),
    );
    setlocale(LC_TIME, $old_locale);
}

# use Data::Dumper; print Dumper @for_upload;

my $rss = new XML::RSS (version => '2.0');

my $pet_cgi = $ENV{GATEWAY_INTERFACE} ? CGI::url() : undef;
$pet_cgi =~ s/rss_upload\.cgi.*/pet.cgi/ if $pet_cgi;

$rss->channel(
    title          => "$CFG{pet_cgi}{group_name} -- ready for upload",
    link           => $pet_cgi || "$CFG{pet_cgi}{group_url}",
    description    => "Packages in the $CFG{pet_cgi}{group_name} repository that need an upload",
    pubDate        => POSIX::strftime("%a, %d %b %Y %T %z", localtime()),
    language       => "en",
    webMaster      => "$CFG{common}{group_email} ($CFG{pet_cgi}{group_name})",
    copyright      => "Copyright " . POSIX::strftime("%Y", localtime()) . " $CFG{pet_cgi}{group_name}",
);

foreach my $pkg ( @for_upload )
{
    my $svn = svn_get();
    my $changelog = $svn->{$pkg->{name}}{text};
    $changelog =~ s/&/&amp;/g;
    $changelog =~ s/'/&apos;/g;
    $changelog =~ s/"/&quot;/g;
    $changelog =~ s/</&lt;/g;  
    $changelog =~ s/>/&gt;/g;  
    $changelog =~ s{\r?\n}{<br/>}g;
    
    $rss->add_item(
       title       => "$pkg->{name}",  
       link        => sprintf($CFG{pet_cgi}{wsvn_url}, $pkg->{name}),
       description => "<pre>" . decode_utf8($changelog) ."</pre>\n",
       guid        => "$CFG{pet_cgi}{group_name}, $pkg->{svn}->{date}",
     );

# use Data::Dumper; print Dumper $pkg;
}

print $rss->as_string;

exit 0;

