About the PET scripts
=====================

There are a bunch of perl modules under the PET namespace, some of them
provide certain common functionality and the others are responsible of data
collection from different sources.

Data collection is completely separated from presentation. There are a couple
of very simple scripts that provide the latter: pet and pet.cgi. They
give more or less the same information on stdout, but the latter is meant to be
used as a CGI script.

The script that controls data collection is fetchdata, which is meant to be run
from a cronjob and/or post-commit hook.

You can ask for basic help with the --help option (not in the cgi version).

All the scripts read from the same configuration file, which you specify with
the --conf option, or with the PET_CONF environment variable. The CGI
script doesn't have the --conf option, obviously.

For a sample configuration file, see the pet.conf-sample file. It is
mostly self-explaining. Don't forget to set a suitable cache_dir, that other
members of your group can write to, and put an absolute path for the template
dir.

Cheat sheet for usual svn layouts:

Layout 1 (python-modules example):
----------------------------------

For a structure like:

svn://svn.debian.org/svn/python-modules/packages/<package>/trunk/

You should use:

[pet_cgi]
wsvn_url = http://svn.debian.org/wsvn/python-modules/packages/%s/trunk

[svn]
repository = svn://svn.debian.org/svn/python-modules/
packages_path = packages
post_path = trunk
track_tags = 1
tags_path = packages
tags_post_path = tags

If you don't have a 'packages' directory under your repository root
use / for both packages_path and tags_path.

Layout 2 (pkg-perl example):
----------------------------

For a structure like:

svn://svn.debian.org/svn/pkg-perl/trunk/<package>/debian/

You should use:

[pet_cgi]
wsvn_url = http://svn.debian.org/wsvn/pkg-perl/trunk/%s

[svn]
repository = svn://svn.debian.org/svn/pkg-perl/
packages_path = trunk
track_tags = 1
tags_path = tags


First run
=========

After configuring, you run the initial download, it could take a long time:

$ <path>/fetchdata --conf <pathtoconf> [-v[v..]] [-j]

-v increases verbosity, and -j enables working in parallel (3 threads).

After that, it will use the cached data if it's not stale, or it will download
what's necessary. You can also specify package directories to avoid updating
the whole database.

Package status in the command line
==================================

With pet you can see in your shell the packages' status, you can have the
full listing or only the specified packages:

$ <path>/pet --conf <pathtoconf> [<package> [<package> ... ]]

asterisk:
 - Version status: Watchfile problem
   + Watch status: DownloadError
   + SVN: 1:1.4.13~dfsg-1 (mangled: 1.4.13) (unreleased: 1:1.4.13~dfsg-2) Archive: 1:1.4.13~dfsg-1 (unstable) Upstream: Unknown (mangled: Unknown)
   + Bugs: #396499, #448171, #433779, #337209, #386114, #399807, #399970, #449706, #381786, #438702, #293751, #353227
(...)

Using the CGI script
====================

Copy or symlink pet.cgi to your project's cgi-bin directory, and copy the
htaccess (renaming it to .htaccess). There you should configure the paths to
find the libraries and the configuration.

Once done that, you will be able to see a nice XHTML version of the status
report. You can write your own template, and switch between them with a GET
parameter: http://..../cgi-bin/pet.cgi?template=my_nice_template

Setting a post-commit hook
==========================

If you want to have the information updated the moment you commit a change, you
can add this lines in your post-commit hook:

REPOS="$1"
REV="$2"

[...]

umask 002
BASE=<path_to_your_local_copy>
PERL5LIB=$BASE $BASE/fetchdata \
        -c <path_to_conf>/pet.conf -r "$REV"

The -r switch sets post-commit mode: it only checks changes in the repository,
and then verifies if it needs to update upstream information for the packages
modified.


Setting a cron job
==================

All the data you downloaded in the first run gets stale after some time, so you
need to run a full check to acquire again what's old (the time to live of each
data source is controlled from the configuration file). So, the best thing to
do is to set up an periodic cron job (once each one or two hours is a good
period, it won't waste bandwith if the data is still current):

$ crontab -l
# m h  dom mon dow   command

BINDIR=<path_to_your_local_copy>
PERL5LIB=<path_to_your_local_copy>

0 * * * * $BINDIR/fetchdata -c <path_to_conf>/pet.conf

$Id$
