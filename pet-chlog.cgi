#!/usr/bin/perl
# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Report packages version states
#
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Released under the terms of the GNU GPL 2
use strict;
use warnings;

use PET::Config qw(read_config %CFG);
use PET::Svn;
use CGI ':fatalsToBrowser';
use CGI;

read_config();

my $cgi = new CGI;


if( $ENV{GATEWAY_INTERFACE} )
{
    print $cgi->header(
        -content_type   => 'text/html; charset=utf-8',
    );
}

my $pkg = $cgi->param('pkg') or exit 0;
my $rel = $cgi->param('rel') || '';

my $svn = svn_get();

my $text = $svn->{$pkg}{ ($rel eq 'rel')?'text' : 'un_text' };

$text =~ s/&/&amp;/g;
$text =~ s/'/&apos;/g;
$text =~ s/"/&quot;/g;
$text =~ s/</&lt;/g;
$text =~ s/>/&gt;/g;
$text =~ s{\r?\n}{<br/>}g;

# replace bug-numbers with links
$text =~ s{
    (               # leading
        ^           # start of string
        |\W         # or non-word
    )
    \#(\d+)         # followed by a bug ID
    \b              # word boundary
}
{$1<a href="http://bugs.debian.org/$2">#$2</a>}xgm;
# treat text as multi-line
# Same for CPAN's RT
$text =~ s{\bCPAN#(\d+)\b}
{<a href="http://rt.cpan.org/Ticket/Display.html?id=$1">CPAN#$1</a>}gm;

print qq(<a style="float: right; margin: 0 0 1pt 1pt; clear: none;"
    href="javascript:async_get( '${pkg}_${rel}_chlog_balloon',
        'pet-chlog.cgi?pkg=$pkg;rel=$rel')">reload</a>\n);
print qq(<code style="white-space: pre">$text</code>);

exit 0;

