# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Module for retrieving data from the SVN repository. It understands SVN
# revisions and uses them instead of timestamps for checking cache validity. It
# parses changelog and watch files.
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package PET::Svn;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = (qw(
    svn_download svn_get svn_get_consolidated
    svndir2pkgname pkgname2svndir get_pkglist get_pkglist_hashref
    ));

use Carp;
use IO::Scalar;
use Digest::MD5 "md5_hex";
use Parse::DebianChangelog;
use PET::Cache;
use PET::Common;
use PET::Config '%CFG';
use PET::DebVersions;
use PET::RepoAccess;
use Parse::DebControl;
use List::Util 'min';

# Returns the list of changed directories
sub svn_download {
    my($force, $revision, @dirlist) = @_;
    $force ||= 0;
    $revision ||= 0;
    debug("svn_download($force, $revision, (@dirlist))");

    die "Missing SVN repository" unless($CFG{repository}{uri});
    my $svnpath = $CFG{repository}{uri};
    my $svn = new PET::RepoAccess(SVN => $svnpath);

    # Starts with a slash
    my $svnpkgpath = "/".$CFG{repository}{packages_path} if(
        $CFG{repository}{packages_path});
    $svnpkgpath =~ s#/+#/#g;
    $svnpkgpath =~ s{/$}{};

    my $svnpkgpostpath = $CFG{repository}{post_path} || "";
    # Always has a slash if not empty
    $svnpkgpostpath =~ s{^/*(.*?)/*$}{/$1} if($svnpkgpostpath);
    $svnpkgpostpath =~ s#/+#/#g;

    my $complete = ! @dirlist;
    unless(@dirlist) {
        info("Retrieving list of directories in SVN");
        my $ret = $svn->listdirs($svnpkgpath);
        carp "$svnpkgpath does not exist\n" unless(ref $ret);
        @dirlist = @$ret;
        info(scalar @dirlist, " directories to process");
    }
    unless($revision) {
        info("Retrieving last revision number from SVN");
        my $ret = $svn->stat($svnpkgpath);
        carp "$svnpkgpath does not exist\n" unless(ref $ret);
        $revision = $ret->{mrev};
    }
    unless($force) {
        my $cdata = read_cache("svn", "", 0);
        if(find_stamp($cdata, "")) {
            my @new = grep({! $cdata->{$_}} @dirlist);
            if(find_stamp($cdata, "") == $revision and not @new
                # don't return if consolidated caches are missing or old
                    and find_stamp(read_cache("consolidated", "pkglist", 0))
                    and find_stamp(read_cache("consolidated", "svn", 0))
            ) {
                info("SVN cache is up-to-date");
                return ();
            }
        } else { # New or old file format
            $force = 1;
        }
    }

    my($pkgdata, @changed) = svn_scanpackages($force, $revision, $svn,
        $svnpkgpath, $svnpkgpostpath, @dirlist);
    if($CFG{repository}{track_tags}) {
        # Starts with a slash
        my $svntagpath = "/" . $CFG{repository}{tags_path} if(
            $CFG{repository}{tags_path});
        $svntagpath =~ s#/+#/#g;
        $svntagpath =~ s{/$}{};

        my $svntagpostpath = $CFG{repository}{tags_post_path} || "";
        # Always has a slash if not empty
        $svntagpostpath =~ s{^/*(.*?)/*$}{/$1} if($svntagpostpath);
        $svntagpostpath =~ s#/+#/#g;

        my $tagdata = svn_scantags($force, $revision, $svn, $svntagpath,
            $svntagpostpath, @dirlist);
        foreach(keys %$pkgdata) {
            $pkgdata->{$_}{tags} = $tagdata->{$_} if($tagdata->{$_});
        }
    }
    # Retain lock
    my $cdata = update_cache("svn", $pkgdata, "", $complete, 1, $revision);

    my @pkglist = grep({ ref $cdata->{$_} and $cdata->{$_}{pkgname} }
        keys(%$cdata));
    my %pkglist;
    foreach(@pkglist) {
        $pkglist{$cdata->{$_}{pkgname}} = {
            svndir => $_,
            binaries => $cdata->{$_}{binaries}
        };
    }
    update_cache("consolidated", \%pkglist, "pkglist", 1, 1);
    my %svn;
    foreach(keys(%$cdata)) {
        next unless ref($cdata->{$_});
        my $pkgname = $cdata->{$_}{pkgname} or next;
        # Shallow copy, it's enough here, but can't be used for anything else
        $svn{$pkgname} = { %{$cdata->{$_}} };
        $svn{$pkgname}{dir} = $_;
        if(defined($svn{$pkgname}{un_text})) {
                # Ignore-Version: ver       # mime-like
                # ignore_version: ver
                # ignore version ver        # free text
            if($svn{$pkgname}{un_text} =~ /^\s*IGNORE[-_ ]VERSION[: ]\s*(\d\S*)/im
                    and $svn{$pkgname}{un_version} eq $1) {
                $svn{$pkgname}{ignore_wip} = 1;
            }
                # waits-for: package version    # rfc-whatever-like (mime)
                # waits_for: package version
                # waits for package version     # free text
            my @changelog_lines = split("\n", $svn{$pkgname}{un_text});
            %{$svn{$pkgname}{waits_list}} = ();
            foreach(@changelog_lines) {
                if($_ =~ /^\s*WAITS[_ -]FOR[: ]\s*(\S+)(?:\s+(\d\S*))?/i) {
                    my $pkg = $1;
                    my $version = $2;
                    $svn{$pkgname}{waits_list}{$pkg} = $version;
                }
            }
        }
        delete $svn{$pkgname}{$_} foreach(
            qw(watch pkgname text un_text long_descr bindata)
        );
    }
    update_cache("consolidated", \%svn, "svn", 1, 0);
    unlock_cache("svn");
    return @changed;
}
sub svn_scantags {
    my($force, $revision, $svn, $prepath, $postpath, @dirlist) = @_;

    # Avoid stupid memory leaks
    $svn = $svn->clone();
    info("Scanning tags from SVN");
    my $cdata;
    my %dirs = map({ ( $_ => 1 ) } @dirlist);
    my %changed;
    if($force) {
        %changed = %dirs;
    } else {
        $cdata = read_cache("svn", "", 0);
        my @candidates;
        # Find oldest non-zero stamp, and force changed for new dirs
        foreach(@dirlist) {
            my $stamp = find_stamp($cdata, $_);
            if($stamp and $cdata->{$_}{tags}) {
                push @candidates, $stamp;
            } else {
                $changed{$_} = 1;
            }
        }
        my $old_rev = min(@candidates);
        if($old_rev) {
            info("Retrieving SVN log since $old_rev");
            my $ret = $svn->get_changes($prepath, $old_rev);
            if(ref $ret) {
                foreach(@$ret) {
                    $changed{$1} = 1 if(m#^/*(.*)/#);
                }
            } else {
                warn("svn log had problems!");
                %changed = %dirs; # fallback
            }
        } else {
            %changed = %dirs;
        }
    }
    my %tags;
    foreach my $dir (@dirlist) {
        unless($changed{$dir}) {
            $tags{$dir} = $cdata->{$dir}{tags};
            next;
        }
        info("Retrieving tags for $dir");
        my $pkghome = "$prepath/$dir$postpath";
        my $tagdirs = $svn->listdirs($pkghome);
        my @tagdirs;
        @tagdirs =  @$tagdirs if(ref $tagdirs);
        map({ s/^(?:debian_)?(?:(?:release|version)_)?//i; s/_/./g } @tagdirs);
        @tagdirs = sort( { deb_compare_nofail($a, $b) } @tagdirs);
        debug("Tags for $dir: @tagdirs");
        $tags{$dir} = \@tagdirs;
    }
    return \%tags;
}
sub svn_scanpackages {
    my($force, $revision, $svn, $prepath, $postpath, @dirlist) = @_;

    info("Scanning packages from SVN");
    my(%svn, %changed);
    my %dirs = map({ ( $_ => 1 ) } @dirlist);
    if($force) {
        %changed = %dirs;
    } else {
        my $cdata = read_cache("svn", "", 0);
        my @candidates;
        # Find oldest non-zero stamp, and force changed for new dirs
        foreach(@dirlist) {
            my $stamp = find_stamp($cdata, $_);
            if($stamp and $cdata->{$_}) {
                push @candidates, $stamp;
            } else {
                $changed{$_} = 1;
            }
        }
        my $old_rev = min(@candidates);
        if($old_rev) {
            # Now search in the SVN log to see if there's any interesting change
            # Remove from list already updated parts of the cache
            info("Retrieving SVN log since $old_rev");
            my $ret = $svn->get_changes($prepath, $old_rev);
            if(ref $ret) {
                foreach(@$ret) {
                    if(m{^/?(.*?)\Q$postpath\E/debian/(?:(?:changelog|control|watch)$|patches/[^/]+$)}) {
                        debug("Changed path: $_");
                        $changed{$1} = 1;
                    }
                }
            } else {
                warn("svn log had problems!");
                %changed = %dirs; # fallback
            }
        } else {
            %changed = %dirs;
        }
        # Copy the not-changed dirs that we want to have the stamp bumped
        foreach(grep({ ! $changed{$_} } @dirlist)) {
            $svn{$_} = $cdata->{$_};
        }
    }
    my @changed = keys %changed;
    foreach my $dir (@changed) {
        # Avoid stupid memory leaks
        $svn = $svn->clone();

        $dir =~ s{^/*(.*?)/*$}{$1};
        my $debdir = "$prepath/$dir$postpath/debian";
        $svn{$dir} = {};

        info("Retrieving control information for $dir");
        my $control = $svn->get_file("$debdir/control");

        unless($control) {
            $svn{$dir}{error} = "MissingControl";
            # Check if it's an invalid dir
            $svn->stat($debdir) and next;
            info("Removing invalid $dir directory");
            $svn{$dir} = {};
            next;
        }

        info("Retrieving changelog for $dir");
        my $changelog = $svn->get_file("$debdir/changelog");

        unless($changelog) {
            $svn{$dir}{error} = "MissingChangelog";
            next;
        }

        # Parse::DebControl hands back a strange structure... A hash-like
        # thing, where [0] includes the debian/control fields for the
        # source package and [1] for the first binary package (and, were 
        # they to exist, [2] and on for the other binary packages - which 
        # we will wisely ignore)
        my ($ctrl_data, $short, $long);
        $control =~ s/^#.*\n//gm; # stripComments looks like nonsense to me
        $ctrl_data = Parse::DebControl->new->parse_mem($control, {
                discardCase => 1 # unreliable if don't
            });
        ($short, $long) = split_description($ctrl_data->[1]{description});

        $svn{$dir}{pkgname} = $ctrl_data->[0]{source};
        my @section = split(/\s*\/\s*/, $ctrl_data->[0]{section});
        unshift @section, "main" unless(@section > 1);
        $svn{$dir}{section} = $section[0];
        $svn{$dir}{subsection} = $section[1];
        $svn{$dir}{maintainer} = [ split(/\s*,\s*/,
            $ctrl_data->[0]{maintainer} || "") ];
        $svn{$dir}{uploaders} = [ split(/\s*,\s*/,
            $ctrl_data->[0]{uploaders} || "") ];
        $svn{$dir}{b_d} = [ split(/\s*,\s*/, $ctrl_data->[0]{b_d} || "") ];
        $svn{$dir}{b_d_i} = [ split(/\s*,\s*/, $ctrl_data->[0]{b_d_i} || "") ];
        $svn{$dir}{std_version} = $ctrl_data->[0]{'standards-version'};
        $svn{$dir}{short_descr} = $short;
        $svn{$dir}{long_descr} = $long;
        my %bins;
        foreach(1..$#$ctrl_data) {
            my $bin = $ctrl_data->[$_];
            my ($shd, $lnd) = split_description($bin->{description});
            $svn{$dir}{bindata}[$_-1] = {
                %$bin,
                short_descr => $shd,
                long_descr => $lnd,
            };
            delete $svn{$dir}{bindata}[$_-1]{description};
            $bins{$bin->{package}} = 1;
            if($bin->{provides}) {
                foreach(split(/\s*,\s*/, $bin->{provides})) {
                    $bins{$_} = 1;
                }
            }
        }
        $svn{$dir}{binaries} = [ sort keys %bins ];
        my $parser = Parse::DebianChangelog->init({
                instring => $changelog });
        my $error = $parser->get_error() or $parser->get_parse_errors();
        if($error) {
            error($error);
            $svn{$dir}{error} = "InvalidChangelog";
            next;
        }

        my($lastchl, $unfinishedchl);
        foreach($parser->data()) {
            if($_->Distribution =~ /^(?:unstable|experimental)$/) {
                $lastchl = $_;
                last;
            }
            if(! $unfinishedchl and $_->Distribution eq "UNRELEASED") {
                $unfinishedchl = $_;
            }
        }
        unless($lastchl or $unfinishedchl) {
            $svn{$dir}{error} = "InvalidChangelog";
            next;
        }
        if($lastchl) {
            $svn{$dir}{version} = $lastchl->Version;
            $svn{$dir}{date}    = $lastchl->Date;
            $svn{$dir}{changer} = $lastchl->Maintainer;
            $svn{$dir}{text}    = join(
                "\n",
                map( $lastchl->$_, qw(Header Changes Trailer) ),
            );
            $svn{$dir}{closes}{$_} = "released" foreach(@{$lastchl->Closes});
        }
        if($unfinishedchl) {
            $svn{$dir}{un_version} = $unfinishedchl->Version;
            $svn{$dir}{un_date}    = $unfinishedchl->Date;
            $svn{$dir}{un_changer} = $unfinishedchl->Maintainer;
            $svn{$dir}{un_text}    = join(
                "\n",
                map( $unfinishedchl->$_, qw(Header Changes Trailer) ),
            );
            $svn{$dir}{closes}{$_} = "unreleased"
            foreach(@{$unfinishedchl->Closes});
        }
        if($svn{$dir}{pkgname} ne $parser->dpkg()->{Source}) {
            $svn{$dir}{error} = "SourceNameMismatch";
            next;
        }

        info("Retrieving watchfile for $dir");
        my $watchdata = $svn->get_file("$debdir/watch");
        if(not $watchdata) {
            if($svn{$dir}{version} and $svn{$dir}{version} !~ /-/) {
                $svn{$dir}{watch_error} = "Native";
            } else {
                $svn{$dir}{watch_error} = "Missing";
            }
        } elsif($watchdata =~ /^#\s*IGNORE\b/im) {
            $svn{$dir}{watch_error} = "Ignore";
        } else {
            my $watch = parse_watch($svn{$dir}{version}, $watchdata);
            # Returns undef on error
            if($watch and @$watch) {
                my @versions = sort({ deb_compare_nofail($a, $b) }
                    grep(defined, map({ $_->{mangled_ver} } @$watch)));

                $svn{$dir}{mangled_ver} = $versions[-1];
                $svn{$dir}{watch} = $watch;
            } else {
                $svn{$dir}{watch_error} = "Invalid";
            }

            # Again for unreleased
            $watch = parse_watch($svn{$dir}{un_version}, $watchdata) if(
                $svn{$dir}{un_version});
            # Returns undef on error
            if($watch and @$watch) {
                my @versions = sort({ deb_compare_nofail($a, $b) }
                    grep(defined, map({ $_->{mangled_ver} } @$watch)));
                $svn{$dir}{mangled_un_ver} = $versions[-1];
            }
        }

        info("Retrieving patches for $dir");
        my $patches = $svn->listfiles("$debdir/patches");
        if(ref $patches) {
            my @patches = @$patches;
            info(scalar @patches, " files in patches/ dir");
            my $ptype;
            my $series = "";
            foreach(@patches) {
                if($_ eq "series") {
                    $ptype = "quilt";
                    debug("Reading quilt series file");
                    my $s = $svn->get_file("$debdir/patches/series");
                    if(defined $s) {
                        $s =~ s/(^|\s+)#.*$//mg;
                        $series .= $s;
                    } else {
                        warn("Problem reading $debdir/patches/series");
                    }
                    last;
                } elsif(/^00list/) {
                    $ptype = "dpatch";
                    debug("Reading dpatch series file");
                    my $s = $svn->get_file("$debdir/patches/$_");
                    # NOTE that this doesn't support CPP-processed lists
                    if(defined $s) {
                        $s =~ s/^#.*$//mg;
                        $series .= $s;
                    } else {
                        warn("Problem reading $debdir/patches/$_");
                    }
                }
            }
            if($ptype) {
                $svn{$dir}{patchsys} = $ptype;
                $svn{$dir}{patches} = [];
            } else {
                warn("Cannot detect patch system for $dir -- ",
                    "asuming simple-patchsys");
                $svn{$dir}{patchsys} = "simple-patchsys";
                $svn{$dir}{patches} = [ map({ "debian/patches/$_" } @patches) ];
                $series = ""; # make next foreach pass-by
            }
            foreach(split(/\n+/, $series)) {
                next unless $_;
                my $p;
                if($ptype eq "quilt") {
                    $p = "patches/$_";
                } elsif($ptype eq "dpatch") {
                    $p = "patches/$_";
                    unless($svn->stat("$debdir/$p")) {
                        $p .= ".dpatch";
                    }
                } else {
                    next;
                }
                # FIXME: do something useful with this
                if($svn->stat("$debdir/$p")) {
                    push @{$svn{$dir}{patches}}, "debian/$p";
                } else {
                    $svn{$dir}{patcherr} = "missing_file";
                    warn("Patchfile $p cannot be found in $dir");
                }
            }
            info(scalar @{$svn{$dir}{patches} || []}, " patches found");
        }
    }
    return(\%svn, @changed);
}
# Returns the hash of svn info. Doesn't download anything.
sub svn_get {
    return read_cache("svn", shift, 0);
}
# Returns the consolidated hash of svn info. Doesn't download anything.
sub svn_get_consolidated {
    my $path = shift || "";
    return read_cache("consolidated", "svn/$path", 0);
}
# Searches the source package name given a svn directory name
# Returns undef if not found
sub svndir2pkgname($) {
    my $dir = shift;
    my $data = read_cache("svn", $dir, 0);
    return $data->{pkgname};
}
# Searches the svn directory name given a source package name
# Returns undef if not found
sub pkgname2svndir($) {
    my $pkg = shift;
    my $data = read_cache("svn", "", 0);
    my @dirs = grep({ ref $data->{$_} and $data->{$_}{pkgname} and
            $data->{$_}{pkgname} eq $pkg } keys %$data);
    return $dirs[0] if(@dirs);
    return undef;
}
# Returns the list of source packages detected in the svn repository
sub get_pkglist {
    my $list = get_pkglist_hashref();
    return keys %$list;
}
sub get_pkglist_hashref {
    my $list = read_cache("consolidated", "pkglist", 0);
    foreach(grep({ /^\// } keys %$list)) {
        delete $list->{$_};
    }
    return $list;
}
# Parses watchfile, returns an arrayref containing one element for each source,
# consisting of the URL spec, an MD5 sum of the line (to detect changes from
# the watch module), the mangled debian version, and a hash of options.
sub parse_watch($$) {
    my($version, $watch) = @_;
    $version ||= '';
    $watch ||= '';
    debug("parse_watch('$version', '...')");

    # Strip epoch and debian release
    $version =~ s/^(?:\d+:)?(.+?)(?:-[^-]+)?$/$1/;

    $watch =~ s/^#.*$//gm;
    $watch =~ s/\\\n//gs;
    $watch =~ s/^\s+//gm;
    $watch =~ s/\s+$//gm;
    my @watch_lines = split(/\n/, $watch);
    @watch_lines = grep((!/^version\s*=/ and !/^\s*$/),
        @watch_lines);

    my @wspecs;
    foreach(@watch_lines) {
        debug("Watch line: $_");

        # opts either contain no spaces, or is enclosed in double-quotes
        my $opts;
        $opts = $1 if(s!^\s*opt(?:ion)?s="([^"]*)"\s+!!
                or s!^\s*opt(?:ion)?s=(\S*)\s+!!);
        debug("Watch line options: $opts") if($opts);

        # several options are separated by comma and commas are not allowed
        # within
        my(@opts, %opts);
        @opts = split(/\s*,\s*/, $opts) if($opts);
        foreach(@opts) {
            next if /^(?:active|passive|pasv)$/;
            /([^=]+)=(.*)/;
            my($k, $v) = ($1, $2);
            debug("Watch option $k: $v");
            if($k eq 'versionmangle') {
                push @{$opts{uversionmangle}}, $v;
                push @{$opts{dversionmangle}}, $v;
            } else {
                push @{$opts{$k}}, $v;
            }
        }
        my $mangled = $version;
        if($version and $opts{dversionmangle}) {
            foreach(split(/;/, join(";", @{$opts{dversionmangle}}))) {
                debug("Executing \$mangled =~ $_");
                eval "\$mangled =~ $_";
                if($@) {
                    error("Invalid watchfile: $@");
                    return undef;
                }
            }
        }
        debug("Mangled version: $mangled");
        push @wspecs, {
            line => $_,
            mangled_ver => $mangled,
            md5 => md5_hex(($opts || "").$_),
            opts => \%opts
        };
    }
    return \@wspecs;
}
sub split_description($) {
    # The 'description' field in debian/control is, IMHO, wrongly handled - Its
    # first line is the short description, and the rest (second to last lines)
    # is the long description. So... Here we just split it, for proper 
    # handling. 
    # 
    # Gets the full description as its only parameter, returns the short and 
    # the long descriptions.
    my ($str, $offset, $short, $long);
    $str = shift;
    $offset = index($str, "\n");
    $short = substr($str, 0, $offset);
    $long = substr($str, $offset+1);
    return ($short, $long);
}

1;
