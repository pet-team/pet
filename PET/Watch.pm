# vim:ts=4:sw=4:et:ai:sts=4
# $Id$
#
# Module for scanning watch files and checking upstream versions.
#
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Copyright Martín Ferrari <martin.ferrari@gmail.com>, 2007
# Released under the terms of the GNU GPL 2

package PET::Watch;
use strict;
use warnings;

our @ISA = "Exporter";
our @EXPORT = qw(watch_download watch_get watch_get_consolidated);

use Compress::Zlib ();
use CPAN::DistnameInfo;
use PET::Cache;
use PET::Common;
use PET::Config '%CFG';
use PET::Svn;
use PET::DebVersions;;
use Fcntl qw(:seek);
use LWP::UserAgent;

my $cpanregex = qr#^((?:http|ftp)://\S*(?:cpan|backpan)\S*)/(dist|modules/by-module|(?:by-)?authors/id)\b#i;

my $ua = new LWP::UserAgent;
$ua->timeout(30);
$ua->env_proxy;
$ua->agent("Debian PET " . $PET::Common::VERSION);

sub watch_download {
    my($force, @pkglist) = @_;
    $force ||= 0;
    debug("watch_download($force, (@pkglist))");

    if($CFG{watch}{use_cpan}) {
        cpan_dist_download($force);
        cpan_index_download($force);
    }
    if(find_stamp(watch_get(), "") == 0) {
        warn("Forcing complete update -- watch cache has old version");
        @pkglist = ();
        $force = 1;
    }
    my $complete;
    if(not @pkglist) {
        $complete = 1;
        @pkglist = get_pkglist();
    }
    my $cdata;
    $cdata = watch_get() unless($force);
    my(%watch, $some_uptodate, @updated);
    foreach my $pkg (@pkglist) {
        debug("Retrieving watchfile from svn for $pkg");
        my $svndata = svn_get(pkgname2svndir($pkg));
        unless(not $svndata->{watch_error}
                and $svndata->{watch} and ref $svndata->{watch}
                and ref $svndata->{watch} eq "ARRAY"
                and @{$svndata->{watch}}) {
            push @updated, $pkg; # Those have no stamp, so force rescan
            next; # Errors will be set in consolidated
        }

        my(@wresult);
        foreach my $wline (@{$svndata->{watch}}) {
            my $md5 = $wline->{md5};
            next unless($md5);
            if(not $force and $cdata->{$md5} and
                $CFG{watch}{ttl} * 60 > time - find_stamp($cdata, $md5)) {
                $some_uptodate = 1;
                next;
            }
            my ($watcherr, %uscand) = uscan($wline->{line}, %{$wline->{opts}});
            unless($watcherr) {
                info("Found: version $uscand{upstream_version} ",
                    "from $uscand{upstream_url} ",
                    "(mangled: $uscand{upstream_mangled})");
            }
            $watch{$md5} = { error => $watcherr, %uscand };
            push @updated, $pkg;

            if(not $watch{$md5}{upstream_mangled}) {
                $watch{$md5}{error} ||= "Error";
            } elsif(not deb_valid($watch{$md5}{upstream_mangled})) {
                $watch{$md5}{error} ||= "InvalidUpstreamVersion";
            } elsif($wline->{mangled_ver}
                    and not deb_valid($wline->{mangled_ver})) {
                $watch{$md5}{error} ||= "InvalidDebianVersion";
            }
            warn("Error while processing $pkg watch file: ",
                $watch{$md5}{error}) if($watch{$md5}{error});
        }
    }
    info("watch: ", scalar @pkglist, " packages scanned");
    if(not @updated and (
            find_stamp(read_cache("consolidated", "pkglist", 0)) <=
            find_stamp(read_cache("consolidated", "watch", 0)))) {
        info("Watch cache is up-to-date");
        return;
    }
    $cdata = update_cache("watch", \%watch, "",
        ($complete and not $some_uptodate), 1);
    # Start consolidated build
    my %watch2;
    unless($complete) {
        # Only re-process updated entries
        @pkglist = @updated;
    }
    foreach my $pkg (@pkglist) {
        my $svndata = svn_get(pkgname2svndir($pkg));
        $watch2{$pkg} = {};

        if($svndata->{watch_error}) {
            $watch2{$pkg}{error} = $svndata->{watch_error};
        } elsif(not $svndata->{watch} or not ref $svndata->{watch}
                or not ref $svndata->{watch} eq "ARRAY") {
            $watch2{$pkg}{error} = "Missing";
        } elsif(not @{$svndata->{watch}}) {
            $watch2{$pkg}{error} = "Empty";
        }
        next if($watch2{$pkg}{error});

        my(@wresult, $error);
        foreach my $wline (@{$svndata->{watch}}) {
            my $md5 = $wline->{md5};
            next unless($md5);
            if($cdata->{$md5}{error}) {
                $error = $cdata->{$md5}{error};
                next;
            }
            next unless($cdata->{$md5}{upstream_mangled});
            if($wline->{mangled_ver}) {
                push @wresult, {
                    diff => deb_compare($wline->{mangled_ver},
                        $cdata->{$md5}{upstream_mangled}),
                    %{$cdata->{$md5}}
                };
            } else { # There's no debian version
                push @wresult, {
                    diff => -1,
                    %{$cdata->{$md5}}
                };
            }
        }
        unless(@wresult) {
            $watch2{$pkg} = { error => $error || "MissingData?" };
            next;
        }
        @wresult = sort({
                deb_compare_nofail($a->{upstream_mangled},
                    $b->{upstream_mangled}) } @wresult);
        my @result;
        if(@result = grep({ $_->{diff} < 0 } @wresult)) {
            $watch2{$pkg} = $result[-1];
        } elsif(@result = grep( { not $_->{diff} } @wresult)) {
            $watch2{$pkg} = $result[0];
        } else {
            $watch2{$pkg} = $wresult[0];
        }
        delete($watch2{$pkg}{diff}) unless($watch2{$pkg}{diff});
        delete($watch2{$pkg}{error}) unless($watch2{$pkg}{error});
    }

    update_cache("consolidated", \%watch2, "watch", $complete, 0);
    unlock_cache("watch");
}
# Returns the hash of bugs. Doesn't download anything.
sub watch_get {
    return read_cache("watch", shift, 0);
}
# Returns the consolidated hash of bugs. Doesn't download anything.
sub watch_get_consolidated {
    my $path = shift || "";
    return read_cache("consolidated", "watch/$path", 0);
}
sub uscan($) {
    my($wline, %opts) = @_;
    info("Processing watch line $wline");

    $wline =~ s{^http://sf\.net/(\S+)}{http://qa.debian.org/watch/sf.php/$1};
    # Fix URIs with no path
    $wline =~ s{^(\w+://[^\s/]+)(\s|$)}{$1/$2};
    unless($wline =~ m{^(?:(?:https?|ftp)://\S+?)/}) {
        warn("Invalid watch line: $wline");
        return("Invalid");
    }
    my @items = split(/\s+/, $wline);

    my($dir, $filter);
    # Either we have single URL/pattern
    # or URL/pattern + extra
    if($items[0] =~ /\(/) {
        # Since '+' is greedy, the second capture has no slashes
        ($dir, $filter) = $items[0] =~ m{^(.+/)(.+)$};
    } elsif(@items >= 2 and $items[1] =~ /\(/) {
        # or, we have a homepage plus pattern
        # (plus optional other non-interesting stuff)
        ($dir, $filter) = @items[0,1];
    }
    unless($dir and $filter) {
        return("Invalid");
    }
    debug("uscan $dir $filter");
    my @vers;
    if($CFG{watch}{use_cpan} and $dir =~ $cpanregex) {
        @vers = cpan_lookup($dir, $filter);
        my $status = shift @vers;
        if($status) {
            warn("CPAN lookup failed for $dir + $filter: $status");
            return $status;
        } elsif(not @vers) {
            warn("CPAN lookup failed for $dir + $filter");
        }
    }
    unless(@vers) {
        @vers = recurse_dirs($filter, $dir);
        my $status = shift @vers;
        return $status || "NotFound" unless(@vers);
    }

    my @mangled;
    foreach my $uver (@vers) {
        push @mangled, $uver->{upstream_version};
        next unless($opts{uversionmangle});
        debug("Mangle option: ", join(", ", @{$opts{uversionmangle}}));
        foreach(split(/;/, join(";", @{$opts{uversionmangle}}))) {
            debug("Executing '\$mangled[-1] =~ $_'");
            if (! safe_replace(\$mangled[-1], $_)) {
                error("Invalid watchfile: $@");
                return("Invalid");
            }
        }
        debug("Mangled version: $mangled[-1]");
    }
    my @order = sort({ deb_compare_nofail($mangled[$a], $mangled[$b]) }
        (0..$#vers));
    return(undef,
        %{$vers[$order[-1]]},
        upstream_dir => $dir,
        upstream_mangled => $mangled[$order[-1]]);
}
sub recurse_dirs($$);
sub recurse_dirs($$) {
    my($filter, $base) = @_;
    debug("recurse_dirs($filter, $base)");

    if($base =~ /\(/) {
        my($newfilt, $staticpart) = ("", "");
        while($base =~ s#/[^/(]*$##) {
            $staticpart = "$&$staticpart";
        }
        $base =~ s#([^/]*\([^/]*)$## or die "Can't happen!!";
        $newfilt = "$1/?";
        debug("After stripping (): $newfilt, $base, remains: $staticpart");
        my ($status, @data) = recurse_dirs($newfilt, $base);
        return $status unless(@data);
        @data = sort({ deb_compare_nofail($a->{upstream_version},
                    $b->{upstream_version}) } @data);
        $base = $data[-1]{upstream_url} . $staticpart;
        debug("Return from recursion: $base");
    }
    unless($base =~ m{(^\w+://[^/]+)(/.*?)/*$}) {
        error("Invalid base: $base");
        return("Invalid");
    }
    my $site = $1;
    my $path = $2;
    my $pattern;
    if($filter =~ m{^/}) {
        $pattern = qr{(?:^\Q$site\E)?$filter};
    } elsif($filter !~ m{^\w+://}) {
        $pattern = qr{(?:(?:^\Q$site\E)?\Q$path\E/)?$filter};
    } else {
        $pattern = $filter;
    }

    debug("Downloading $base");
    my $res = $ua->get($base);
    unless($res->is_success) {
        error("Unable to get $base: " . $res->message());
        return ("NotFound") if($res->code == 404);
        return ("DownloadError");
    }
    my $page = $res->decoded_content();
    unless($page) {
        error("Unable to get $base: empty page");
        return ("DownloadError");
    }
    $page =~ s/<!--.*?-->//gs;
    $page =~ s/\n+/ /gs;

    my @candidates;
    if($base =~ /^ftp/) {
        @candidates = split(/\s+/, $page);
    } else {
        @candidates = grep(defined, ($page =~
                m{<a\s[^>]*href\s*=\s*(?:"([^"]+)"|'([^']+)'|([^"][^\s>]+))}gi));
    }
    my @vers;
    foreach my $url (grep(m{^$pattern$}, @candidates)) {
        my $ver = join(".", ($url =~ m{^$pattern$}));
        if($ver =~ s#/+$##) { # Can't find a better way
            $url =~ s#\Q$&\E$##; # Remove the same
        }
        if($url =~ m{^/}) {
            $url = $site . $url;
        } elsif($url !~ m{^\w+://}) {
            $url = $site . $path . "/" . $url;
        }
        push @vers, {
            upstream_version => $ver,
            upstream_url => $url };
    }
    debug("Versions found: ", join(", ", map({ $_->{upstream_version} }
                @vers)));
    return(undef, @vers);
}

sub cpan_lookup($$) {
    my($dir, $filter) = @_;

    $dir =~ $cpanregex or return ();
    my $base = $1;
    my $type = $2;
    $dir =~ s{/+$}{};
    my $origdir = $dir;

    $type =~ s/.*(dist|modules|authors).*/$1/ or return ();
    my $cpan;
    if($type eq "dist") {
        $filter =~ s/.*\///;
        $cpan = cpan_dist_download();
    } else {
        $cpan = cpan_index_download()->{$type};
    }
    $dir =~ s/$cpanregex//i;
    $dir =~ s{^/+}{};
    debug("Looking for $dir + $filter into CPAN $type cache");
    #return ("NotFound") unless(exists($cpan->{$dir}));
    # Allow this to gracefully degrade to a normal uscan check
    return () unless(ref($cpan) eq "HASH" && exists($cpan->{$dir}));

    my @res;
    foreach(keys %{$cpan->{$dir}}) {
        next unless ($_ =~ $filter);
        my $filt_ver = $1;
        if($type eq "dist") {
            my $cpan_ver = $cpan->{$dir}{$_}{version};
            if($filt_ver ne $cpan_ver) {
                # Try to remove initial "v"s, if any
                $cpan_ver =~ s/^v//;
            }
            if($filt_ver ne $cpan_ver) {
                warn("Version mismatch: uscan says $filt_ver, ",
                    "cpan says $cpan_ver");
                return ("VersionMismatch");
            }
        }
        push @res, {
            upstream_version => $filt_ver,
            upstream_url => (
                $type eq "dist" ?
                "$base/CPAN/authors/id/" . $cpan->{$dir}{$_}{path} :
                "$origdir/$_"
            )
        };
    }
    # Allow this to gracefully degrade to a normal uscan check
    #return ("NotFound") unless(@res);
    return (undef, @res);
}
sub cpan_dist_download(;$) {
    my $force = shift;
    unless($force) {
        my $cpan = read_cache("cpan_dists", "", 0);
        if($CFG{watch}{cpan_ttl} * 60 > time - find_stamp($cpan, "")) {
            return $cpan;
        }
    }

    my $url = $CFG{watch}{cpan_mirror} . "/modules/02packages.details.txt.gz";
    info("Rebuilding CPAN dists cache from $url");
    open(TMP, "+>", undef) or die $!;
    my $res = $ua->get($url, ":content_cb" => sub {
            print TMP $_[0] or die $!;
        });
    unless($res->is_success()) {
        warn "Can't download $url: " . $res->message();
        return 0;
    }
    seek(TMP, 0, SEEK_SET) or die "Can't seek: $!\n";
    my $gz = Compress::Zlib::gzopen(\*TMP, "rb")
        or die "Can't open compressed file: $!\n";

    my $data;
    open($data, "+>", undef) or die $!;
    my $buffer = " " x 4096;
    my $bytes;
    while(($bytes = $gz->gzread($buffer)) > 0) {
        print $data $buffer;
    }
    die $gz->gzerror if($bytes < 0);
    close TMP;
    #my $z = new IO::Uncompress::Gunzip(\$data);

    seek($data, 0, SEEK_SET) or die "Can't seek: $!\n";

    # Skip header
    while(<$data>) {
        chomp;
        last if(/^$/);
    }
    my $cpan = {};
    while(<$data>) {
        chomp;
        my $tarball = (split)[2];
        my $distinfo = new CPAN::DistnameInfo($tarball);
#       next if($distinfo->maturity() eq "developer");
        my $distname = $distinfo->dist();
        unless($distname) {
            info("Invalid CPAN distribution: $tarball");
            next;
        }
        my $version = $distinfo->version();
        my $filename = $distinfo->filename();

        $cpan->{$distname}{$filename} = {
            path => $tarball,
            version => $version
        };
    }
    close $data;
    update_cache("cpan_dists", $cpan, "", 1);
    return $cpan;
}
sub cpan_index_download(;$) {
    my $force = shift;
    unless($force) {
        my $cpan = read_cache("cpan_index", "", 0);
        if($CFG{watch}{cpan_ttl} * 60 > time - find_stamp($cpan, "")) {
            return $cpan;
        }
    }

    my $url = $CFG{watch}{cpan_mirror} . "/indices/ls-lR.gz";
    info("Rebuilding CPAN indices cache from $url");
    open(TMP, "+>", undef) or die $!;
    my $res = $ua->get($url, ":content_cb" => sub {
            print TMP $_[0] or die $!;
        });
    unless($res->is_success()) {
        warn "Can't download $url: " . $res->message();
        return 0;
    }
    seek(TMP, 0, SEEK_SET) or die "Can't seek: $!\n";
    my $gz = Compress::Zlib::gzopen(\*TMP, "rb")
        or die "Can't open compressed file: $!\n";

    my $data;
    open($data, "+>", undef) or die $!;
    my $buffer = " " x 4096;
    my $bytes;
    while(($bytes = $gz->gzread($buffer)) > 0) {
        print $data $buffer;
    }
    die $gz->gzerror if($bytes < 0);
    close TMP;
    #my $z = new IO::Uncompress::Gunzip(\$data);

    seek($data, 0, SEEK_SET) or die "Can't seek: $!\n";

    my $cpan = {};
    my($dir, $type);
    while(<$data>) {
        chomp;
        if(/^(.+):$/) {
            my $subdir = $1;
            $type = undef;
            $subdir =~ m{/.*(authors/id|modules/by-module)/+(.*?)/*$} or next;
            $dir = $2;
            $1 =~ /(authors|modules)/ and $type = $1;
            next;
        }
        next unless($type and /^[-l]r.....r/);
        s/ -> .*//;
        my $file = (split)[8];
        $file =~ m{\.(?:bz2|gz|zip|pl|pm|tar|tgz)$}i or next;
        $cpan->{$type}{$dir}{$file} = 1;
    }
    close $data;
    update_cache("cpan_index", $cpan, "", 1);
    return $cpan;
}

sub quoted_regex_parse {
    my $pattern = shift;
    my %closers = ('{', '}', '[', ']', '(', ')', '<', '>');

    $pattern =~ /^(s|tr|y)(.)(.*)$/;
    my ($sep, $rest) = ($2, $3 || '');
    my $closer = $closers{$sep};

    my $parsed_ok = 1;
    my $regexp = '';
    my $replacement = '';
    my $flags = '';
    my $open = 1;
    my $last_was_escape = 0;
    my $in_replacement = 0;

    for my $char (split //, $rest) {
        if ($char eq $sep and ! $last_was_escape) {
            $open++;
            if ($open == 1) {
                if ($in_replacement) {
                    # Separator after end of replacement
                    $parsed_ok = 0;
                    last;
                } else {
                    $in_replacement = 1;
                }
            } else {
                if ($open > 1) {
                    if ($in_replacement) {
                        $replacement .= $char;
                    } else {
                        $regexp .= $char;
                    }
                }
            }
        } elsif ($char eq $closer and ! $last_was_escape) {
            $open--;
            if ($open) {
                if ($in_replacement) {
                    $replacement .= $char;
                } else {
                    $regexp .= $char;
                }
            } elsif ($open < 0) {
                $parsed_ok = 0;
                last;
            }
        } else {
            if ($in_replacement) {
                if ($open) {
                    $replacement .= $char;
                } else {
                    $flags .= $char;
                }
            } else {
                $regexp .= $char;
            }
        }
        # Don't treat \\ as an escape
        $last_was_escape = ($char eq '\\' and ! $last_was_escape);
    }

    $parsed_ok = 0 unless $in_replacement and $open == 0;

    return ($parsed_ok, $regexp, $replacement, $flags);
}

sub safe_replace {
    my ($in, $pat) = @_;
    $pat =~ s/^\s*(.*?)\s*$/$1/;

    $pat =~ /^(s|tr|y)(.)/;
    my ($op, $sep) = ($1, $2 || '');
    my $esc = "\Q$sep\E";
    my ($parsed_ok, $regexp, $replacement, $flags);

    if ($sep eq '{' or $sep eq '(' or $sep eq '[' or $sep eq '<') {
        ($parsed_ok, $regexp, $replacement, $flags) = quoted_regex_parse($pat);

        return 0 unless $parsed_ok;
    } elsif ($pat =~ /^(?:s|tr|y)$esc((?:\\.|[^\\$esc])*)$esc((?:\\.|[^\\$esc])*)$esc([a-z]*)$/) {
        ($regexp, $replacement, $flags) = ($1, $2, $3);
    } else {
        return 0;
    }

    my $safeflags = $flags;
    if ($op eq 'tr' or $op eq 'y') {
        $safeflags =~ tr/cds//cd;
        return 0 if $safeflags ne $flags;

        $regexp =~ s/\\(.)/$1/g;
        $replacement =~ s/\\(.)/$1/g;

        $regexp =~ s/([^-])/'\\x'  . unpack 'H*', $1/ge;
        $replacement =~ s/([^-])/'\\x'  . unpack 'H*', $1/ge;

        eval "\$\$in =~ tr<$regexp><$replacement>$flags;";

        if ($@) {
            return 0;
        } else {
            return 1;
        }
    } else {
        $safeflags =~ tr/gix//cd;
        return 0 if $safeflags ne $flags;

        my $global = ($flags =~ s/g//);
        $flags = "(?$flags)" if length $flags;

        my $slashg;
        if($regexp =~ /(?<!\\)(\\\\)*\\G/) {
            $slashg = 1;
            # if it's not initial, it is too dangerous
            return 0 if($regexp =~ /^.*[^\\](\\\\)*\\G/);
        }

        # Behave like Perl and treat e.g. "\." in replacement as "."
        # We allow the case escape characters to remain and
        # process them later
        $replacement =~ s/(^|[^\\])\\([^luLUE])/$1$2/g;

        # Unescape escaped separator characters
        $replacement =~ s/\\\Q$sep\E/$sep/g;
        # If bracketing quotes were used, also unescape the
        # closing version
        $replacement =~ s/\\\Q}\E/}/g if $sep eq '{';
        $replacement =~ s/\\\Q]\E/]/g if $sep eq '[';
        $replacement =~ s/\\\Q)\E/)/g if $sep eq '(';
        $replacement =~ s/\\\Q>\E/>/g if $sep eq '<';

        # The replacement below will modify $replacement so keep
        # a copy. We'll need to restore it to the current value if
        # the global flag was set on the input pattern.
        my $orig_replacement = $replacement;

        my ($first, $last, $pos, $zerowidth, $matched, @captures) = (0, -1, 0);
        while (1) {
            eval {
                # handle errors due to unsafe constructs in $regexp
                no re 'eval';

                # restore position
                pos($$in) = $pos if($pos);
                if($zerowidth) {
                    # previous match was a zero-width match, simulate it to set
                    # the internal flag that avoids the infinite loop
                    $$in =~ /()/g;
                }
                # Need to use /g to make it use and save pos()
                $matched = ($$in =~ /$flags$regexp/g);
                if ($matched) {
                    my $oldpos = $pos;
                    # save position and size of the match
                    $pos = pos($$in);
                    ($first, $last) = ($-[0], $+[0]);
                    if($slashg) {
                        # \G in the match, weird things can happen
                        $zerowidth = ($pos == $oldpos);
                        # For example, matching without a match
                        $matched = 0 if(not defined $first
                                or not defined $last);
                    } else {
                        $zerowidth = ($last - $first == 0);
                    }
                    for my $i (0..$#-) {
                        $captures[$i] = substr $$in, $-[$i], $+[$i] - $-[$i];
                    }
                }
            };
            return 0 if $@;

            # No match; leave the original string  untouched but return
            # success as there was nothing wrong with the pattern
            return 1 unless $matched;

            # Replace $X
            $replacement =~ s/[\$\\](\d)/defined $captures[$1] ? $captures[$1] : ''/ge;
            $replacement =~ s/\$\{(\d)\}/defined $captures[$1] ? $captures[$1] : ''/ge;
            $replacement =~ s/\$&/$captures[0]/g;

            # Make \l etc escapes work
            $replacement =~ s/\\l(.)/lc $1/e;
            $replacement =~ s/\\L(.*?)(\\E|\z)/lc $1/e;
            $replacement =~ s/\\u(.)/uc $1/e;
            $replacement =~ s/\\U(.*?)(\\E|\z)/uc $1/e;

            # Actually do the replacement
            substr $$in, $first, $last - $first, $replacement;
            # Update position
            $pos += length($replacement)- ($last - $first);

            last unless($global);
            $replacement = $orig_replacement;
        }
        return 1;
    }
}

1;
